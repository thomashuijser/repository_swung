﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    float value;

    Vector2 startPos;

    public float speed;

    public Transform right;
    public Transform left;

    bool leftTurn;

    float random;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
         random = Random.Range(0, 3);
    }

    // Update is called once per frame
    void Update()
    {
        SetWaterOffset();
    }

    void SetWaterOffset()
    {
        float val = transform.position.x - startPos.x;
      if(Mathf.Abs(val) >= 10)
        {
            value = 0;
        }
        value += speed * Time.deltaTime;
        transform.position = new Vector2(startPos.x + value, startPos.y + 0.02f + (Mathf.Sin(Time.time + random) / 10));
    }

    void TeleportSide()
    {
        if (leftTurn)
        {
            
        }
    }
}
