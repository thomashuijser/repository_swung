﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    EnemyStats stats;
    EnemyPathfinder pathfinder;
    public Directions direction;

    float idleHeight;
    public float speed;
    public float maxDistance;

    bool launchCharacter;

    Transform target;
    Vector2 launchDirection;

    DamageObject damageObject;
    // Start is called before the first frame update
    void Start()
    {

        damageObject = GetComponentInChildren<DamageObject>();
        stats = GetComponent<EnemyStats>();
        pathfinder = GetComponent<EnemyPathfinder>();

        idleHeight = transform.position.y;

        StartCoroutine(Attack());
    }

    // Update is called once per frame
    void Update()
    {
        //GROSS!! only here for temp. fixing of enemy flying out of scene...
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        CheckCollision();
        if (pathfinder.target && stats.canWalk && !launchCharacter) Idlemove();

        if (launchCharacter) Launch();
    }

    void CheckCollision()
    {
        if(damageObject.hitTag == "Player")
        {
            launchCharacter = false;
            stats.canWalk = true;
            damageObject.hitTag = null;
        }
        else if(damageObject.hitTag == "Tile")
        {
            StartCoroutine(Freeze(2));
            damageObject.hitTag = null;
        }
    }

    void IdleMovement()
    {
        Vector2 pos = new Vector2(transform.position.x + ((int)direction * Time.deltaTime * speed), pathfinder.target.position.y + 2 + Mathf.PerlinNoise(1, transform.position.x));

        if (Mathf.Abs(pos.x - pathfinder.target.position.x) < maxDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, pos, 10f * Time.deltaTime);
        }
        else if (direction == Directions.Left)
        {
            direction = Directions.Right;
            stats.spriteAnimation.spriteRenderer.flipX = true;
        }
        else if (direction == Directions.Right)
        {
            direction = Directions.Left;
            stats.spriteAnimation.spriteRenderer.flipX = false;
        }
    }

    void Launch()
    {
        transform.position = Vector2.MoveTowards(transform.position, launchDirection, 10f * Time.deltaTime);
    }
    
    void Idlemove()
    {
        Vector2 pos = new Vector2(transform.position.x + ((int)direction * Time.deltaTime * speed), pathfinder.target.position.y + 2 + Mathf.PerlinNoise(1, transform.position.x));

        if (direction == Directions.Left && pos.x - pathfinder.target.position.x > -maxDistance) {
            transform.position = Vector2.MoveTowards(transform.position, pos, 0.05f);
        }
        else
        {
            direction = Directions.Right;
            stats.spriteAnimation.spriteRenderer.flipX = true;
        }
        if (direction == Directions.Right && pos.x - pathfinder.target.position.x < maxDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, pos, 0.05f);
        }
        else
        {
            direction = Directions.Left;
            stats.spriteAnimation.spriteRenderer.flipX = false;
        }
    }

    IEnumerator Freeze(float time)
    {
        yield return new WaitForSeconds(time);
        launchCharacter = false;
        stats.canWalk = true;
        damageObject.hitTag = null;
    }

    IEnumerator Attack()
    {
        while (true)
        {
               yield return new WaitForSeconds(Random.Range(5,10));
            if (pathfinder.target && stats.canWalk)
            {
                launchDirection = pathfinder.target.position;
                StartCoroutine(stats.StartCharging(20));
                Debug.Log("attacking!");

                yield return new WaitUntil(() =>stats.fullcharge);
                launchCharacter = true;
                stats.fullcharge = false;
                stats.Charging = false;
            }
        }
    }
}
