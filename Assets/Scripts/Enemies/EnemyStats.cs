﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public string name;
    public float health;
    public int movementSpeed;

    public bool canWalk;
    public bool canJump;
    public float jumpStrength;
    public bool protect;

    public bool ignoreSword;
        
    public bool freeze;

    public bool Charging;
    public bool fullcharge;
    public bool attack;

    public bool inCooldown;

    public Color bloodColor;

    public ParticleSystem bloodParticles;
    public ParticleSystem poofParticles;
    public ParticleSystem slashparticles;

    SpriteRenderer spriterenderer;
    EnemyPathfinder ep;

    public Sprite[] idleSprites;
    public Sprite[] DeathSprites;
    public Sprite[] AttackSprites;

    [HideInInspector]
    public SpriteAnimation spriteAnimation;

    AudioSource audioSource;

    public AudioClip deathClip;
    public AudioClip hitClip;


    [Tooltip("the cooldown time before an action is undergone")]
    public float actionCooldown;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        bloodParticles.startColor = bloodColor;
        spriterenderer = GetComponentInChildren<SpriteRenderer>();
        ep = GetComponent<EnemyPathfinder>();

        spriteAnimation = GetComponentInChildren<SpriteAnimation>();
    }
    private void Update()
    {
        FlipSprite();
        if (spritePos == Vector2.zero) spritePos = spriteAnimation.spriteRenderer.transform.localPosition;
    }
    public void ReceiveDamage(float amount)
    {
        if (protect) return;

        StopCoroutine(HitAnimation());
        StartCoroutine(HitAnimation());
       // if (inCooldown) return;

        StartCoroutine(HitCooldown());
        health -= amount;
        bloodParticles.Play();

        if (health <= 0)
        {
            //dead
            PlayAudio(deathClip);
            StartCoroutine(deathAnimation());
        }
        else
        {
            PlayAudio(hitClip);
        }
    }


    float flashAmount;
    IEnumerator HitAnimation()
    {
        slashparticles.Play();
        flashAmount = 0;
        while (flashAmount <= 1f)
        {
            yield return new WaitForSeconds(0.02f);
            flashAmount += 0.1f;
            spriterenderer.material.SetFloat("_FlashAmount", flashAmount);
        }
        while (flashAmount >= 0f)
        {
            yield return new WaitForSeconds(0.02f);
            flashAmount -= 0.1f;
            spriterenderer.material.SetFloat("_FlashAmount", flashAmount);
        }
        spriterenderer.material.SetFloat("_FlashAmount", 0);
        flashAmount = 0;
    }

    IEnumerator deathAnimation()
    {
        if(DeathSprites.Length != 0)
        {
            spriteAnimation.ChangeSprites(DeathSprites, true);
            spriteAnimation.loop = false;
        }
        freeze = true;

        ep.rb.velocity = new Vector2(0,0);
        if (DeathSprites.Length != 0)  yield return new WaitUntil(() => spriteAnimation.finishedNoLoop == true);
        
        poofParticles.Play();
        yield return new WaitForSeconds(0.2f);
        poofParticles.gameObject.transform.SetParent(null);
        
        CharacterPathfinder.spottedEnemies.Remove(this);
        gameObject.SetActive(false);
    }

    IEnumerator HitCooldown()
    {
        inCooldown = true;
        yield return new WaitForSeconds(1);
        inCooldown = false;
    }

    Vector2 spritePos;
    //shakes a sprite
    void Shake(Transform t, Vector2 p, float amount)
    {
        Vector2 shakeposition = new Vector2(p.x + Random.insideUnitSphere.x * amount, p.y + Random.insideUnitSphere.x * (amount / 4));
        t.localPosition = Vector2.Lerp(t.localPosition, shakeposition, 0.5f);
    }
    public IEnumerator StartCharging(int shakes)
    {
        canWalk = false;
        Charging = true;
        for(int i = 0; i <= shakes; i++)
        {
            yield return new WaitForSeconds(0.05f);
            Shake(spriteAnimation.spriteRenderer.transform, spritePos, 0.2f);
        }
        canWalk = true;
        fullcharge = true;
    }

    void FlipSprite()
    {

        float distance = transform.position.x - GameHandler.playerTransform.position.x;
        if (distance < 0)
        {
            if (!spriterenderer.flipX)  spriterenderer.flipX = true;
        }
        else
        {
            if (spriterenderer.flipX) spriterenderer.flipX = false;
        }
    }

    void PlayAudio(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }
}
