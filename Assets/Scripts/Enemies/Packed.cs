﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Packed : MonoBehaviour
{
    public EnemyPathfinder[] packed;

    [HideInInspector]
    public bool notifiedPacked;

    void Start()
    {
        foreach (EnemyPathfinder ef in packed)
        {
            ef.packed = this;
        }
    }

    public void SetTarget(Transform target)
    {
        foreach(EnemyPathfinder ef in packed)
        {
            ef.target = target;
        }
    }
}
