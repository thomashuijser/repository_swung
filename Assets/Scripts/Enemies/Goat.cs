﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goat : MonoBehaviour
{

    EnemyPathfinder enemyPathfinder;
    Rigidbody2D rb;

    bool foundTarget;

    public float chargeTime;

    // Start is called before the first frame update
    void Start()
    {
        enemyPathfinder = GetComponent<EnemyPathfinder>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Search();
    }

    void Attack()
    {
        rb.velocity = new Vector2(1, 0);
    }

    void Search()
    {
        foreach(string s in enemyPathfinder.directionHitTag)
        {
            if (s == "Player")
            {
                if (Charge())
                {
                    StartCoroutine(StartAttack());
                    chargeTime = 0;
                }
            }
        }
    }

    IEnumerator StartAttack()
    {
        rb.velocity = new Vector2(0, 1);
        yield return new WaitForSeconds(0.5f);
        foundTarget = true;
    }

    bool Charge()
    {
        if(chargeTime < 100)
        {
            Debug.Log(chargeTime  * 10);
            chargeTime += Time.deltaTime;
            return false;

        }
        else
        {
            return false;
        }
    }
}
