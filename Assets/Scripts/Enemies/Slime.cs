﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    EnemyStats stats;
    EnemyPathfinder pathfinder;

    float curCooldown;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<EnemyStats>();
        pathfinder = GetComponent<EnemyPathfinder>();
        curCooldown = Random.Range(stats.actionCooldown - 1, stats.actionCooldown + 1);
    }

    // Update is called once per frame
    void Update()
    {
    if(!stats.freeze) Movement();
    }

    void Movement()
    {
        if (pathfinder.target)
        {
            if (Delay())
            {
                Jump();
            }
        }
    }

    bool Delay()
    {
        if (curCooldown <= 0)
        {
            curCooldown = stats.actionCooldown;
            return true;
        }
        else
        {
            curCooldown -= Time.deltaTime;
            return false;
        }
    }
    void Jump()
    {
        var heading = pathfinder.target.position - transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;

        float dir = heading.y;

        if(heading.x > 0)
        {
            pathfinder.rb.velocity = new Vector2(1.3f, direction.y + 5);
        }
        else
        {
            pathfinder.rb.velocity = new Vector2(-1.3f, direction.y + 5);
        }

    }
}
