﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBird : MonoBehaviour
{

    Rigidbody2D rb;
    public EnemyPathfinder enemyPathFinder;

    public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        enemyPathFinder = GetComponent<EnemyPathfinder>();
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyPathFinder.target)
        Movement();
    }

    void Movement()
    {
        var heading = GameHandler.playerTransform.position - transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;
        Vector2 velocity = direction * 1.5f;
        rb.velocity = Vector2.MoveTowards(rb.velocity, velocity, Time.deltaTime * 10);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sword") return;

        Instantiate(explosion, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Sword") return;

        Instantiate(explosion, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }
}
