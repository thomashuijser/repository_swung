﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathfinder : MonoBehaviour
{
    [Tooltip("can the enemy move?")]
    public bool Walk;
    [Tooltip("can the enemy jump")]
    public bool Jump;
    [Tooltip("if the character can receive knockback")]
    public bool receiveKnockback = true;

    public bool startWithTarget;

    public int sightDistance;

    public int directions;

    float[] directionsValue;
    public string[] directionHitTag;

    public LayerMask layermask;

    public Transform target;

    public Rigidbody2D rb;

    [HideInInspector]
    public Packed packed;

    private void OnValidate()
    {
        if (directions % 2 != 0) directions++;
    }
    // Start is called before the first frame update
    void Start()
    {
        directionHitTag = new string[directions];
        directionsValue = new float[directions];
        rb = GetComponent<Rigidbody2D>();

        if (startWithTarget) target = GameHandler.playerTransform;
    }

    // Update is called once per frame
    public void Update()
    {
        if (packed) NotifyPacked();
        RaycastAngles();

        NoticeDistance();
    }

    void NotifyPacked()
    {
        if (target && !packed.notifiedPacked)
        {
            packed.SetTarget(target);
            packed.notifiedPacked = true;
        }
    }

    void RaycastAngles()
    {
        int angle = 0;

        for (int i = 0; i < directionsValue.Length; i++)
        {
            directionsValue[i] = Raycasting(VectorFromAngle(angle), transform.position, i);
            angle -= 360 / (directionsValue.Length);
        }
    }

    void NoticeDistance()
    {
        float distance = Vector2.Distance(transform.position, GameHandler.playerTransform.position);

        if(distance <= sightDistance)
        {
            if (!target) target = GameHandler.playerTransform;
        }
    }

    void AttackAngle()
    {

    }

    float Raycasting(Vector2 direction, Vector3 position, int directionCount)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, direction, sightDistance, layermask);

        if (hit.collider != null)
        {
            directionHitTag[directionCount] = hit.transform.tag;

            Debug.DrawLine(transform.position, hit.transform.position, Color.blue);
            // needs to be implement
            // directionHitTag[directionCount] = hit.collider.tag;
            if (hit.distance < sightDistance) {
                if(hit.collider.tag == "Player")
                {
                    Debug.DrawRay(position, direction * hit.distance, Color.blue);
                    if (target == null) target = hit.collider.transform;
                }
                Debug.DrawRay(position, direction * hit.distance, Color.green);
            }
            else
                    Debug.DrawRay(position, direction * hit.distance, Color.yellow);

                return hit.distance;
            
        }
        else
        {
            Debug.DrawRay(position, direction * sightDistance, Color.red);
        }
        return sightDistance * 10;
    }

    Vector2 VectorFromAngle(float theta)
    {
        Vector2 dir = (Quaternion.Euler(0, 0, theta) * Vector2.right);
        return dir;
    }
}
