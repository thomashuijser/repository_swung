﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAI : MonoBehaviour
{
    public Animator animator;

    [Range(0,1)]
    public float value;

    public SpriteRenderer head;
    public Sprite headSprite;
    public Sprite headAttack;

    public GameObject fire;

    public Spawner fireSpawner;

    public float destroyDelay = 1.8f;

    bool started;

    [Range(0, 1)]
    public float disolvePower;

    public SpriteRenderer[] s;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        StartCoroutine(StartAttack());

        //t.material.SetFloat("_EmissionThickness", emissionThickness);
    }

    // Update is called once per frame
    void Update()
    {
        if (s.Length > 0)
        {
            foreach(SpriteRenderer sa in s)
            {
                sa.material.SetFloat("_DissolvePower", disolvePower);
            }
        }

        if (started)
        {
            CalculateLookPosition();
            animator.PlayInFixedTime("Look", -1, value);


            if (animator.GetBool("Deactivate"))
            {
                head.sprite = headAttack;
                started = false;
                StartCoroutine(Destroyed());
            }
        }
    }


    IEnumerator Destroyed()
    {
        List<GameObject> fireballs = fireSpawner.activatedEnemies;
        fireSpawner.enabled = false;
        Destroy(fireSpawner);
        
        foreach(GameObject g in fireballs)
        {
            g.SetActive(false);
        }
        yield return new WaitForSeconds(destroyDelay);
        transform.parent.gameObject.SetActive(false);
    }

    IEnumerator StartAttack()
    {
        yield return new WaitForSeconds(2.05f);
        started = true;
        StartCoroutine(AttackLoop());
    
    }

    IEnumerator AttackLoop()
    {
        while (started)
        {
            fire.SetActive(false);
            head.sprite = headSprite;
            yield return new WaitForSeconds(Random.Range(2, 3));
            head.sprite = headAttack;
            yield return new WaitForSeconds(0.2f);
            fire.SetActive(true);
            fire.GetComponent<Spawner>().restart = true;
            yield return new WaitForSeconds(3f);
        }
    }

    void CalculateLookPosition()
    {
        float characterY = GameHandler.playerTransform.position.y;

        characterY += 1.5f;

        characterY = characterY / 4.5f;

        value = Mathf.MoveTowards(value, Mathf.Clamp(characterY, 0f, 0.999f), 0.005f);
    }
}
