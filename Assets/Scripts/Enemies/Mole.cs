﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mole : MonoBehaviour
{
    SpriteRenderer sprite;
    SpriteAnimation spriteAnimation;
    public float retreatDistance;
    public Transform arm;
    SpriteRenderer bowSprite;
    public GameObject projectile;
    
    EnemyPathfinder pathfinder;
    EnemyStats enemyStats;

    public GameObject retreatObject;
    public string idleSprites;
    public string retreatSprites;
    public string returnSprites;


    public Sprite[] bowSprites;
    public LayerMask mask;
    
    bool aiming = true;

    public bool isAttacking;

    // Start is called before the first frame update
    void Start()
    {
        bowSprite = arm.GetComponentInChildren<SpriteRenderer>();
        enemyStats = GetComponent<EnemyStats>();
        spriteAnimation = GetComponentInChildren<SpriteAnimation>();
        pathfinder = GetComponent<EnemyPathfinder>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Retreat();

        if (pathfinder.target && !isAttacking)
        {
            StartCoroutine(Fire());
            isAttacking = true;
        }

        if (aiming) RotateToCharacter();
    }


    Vector2 dir;
    void RotateToCharacter()
    {
        var direction = (Vector2)(arm.position - GameHandler.playerTransform.position).normalized;
        dir = direction;
        arm.up = direction;
    }

    //checks if the mole has to be reatreated
    void Retreat()
    {
        if (CheckPlayerDistance())
        {
            if (retreatObject.activeSelf)
            {
                spriteAnimation.ChangeSpritesLocation(retreatSprites, true);
                spriteAnimation.loop = false;
                enemyStats.protect = true;
                StopCoroutine(FinishNoLoop());
                retreatObject.SetActive(false);
            }
        }
        else
        {
            if (!retreatObject.activeSelf)
            {
                spriteAnimation.ChangeSpritesLocation(returnSprites, true);
                spriteAnimation.loop = false;
                StartCoroutine(FinishNoLoop());
                enemyStats.protect = false;
                retreatObject.SetActive(true);
            }
        }

        if (!retreatObject.activeSelf && spriteAnimation.SpriteLocation == idleSprites) 
        {
            spriteAnimation.loop = false;
            spriteAnimation.ChangeSpritesLocation(retreatSprites, true);
        }
    }


    bool CheckPlayerDistance()
    {
        float distance = Vector2.Distance(transform.position, GameHandler.swordTransform.position);

        return distance < retreatDistance;
    }

    bool CanSeePlayer()
    {
        RaycastHit2D hit = Physics2D.Raycast(arm.position, -dir, 25, mask);

        if (hit.transform.tag == "Player") return true;

        return false;
    }

    IEnumerator Fire()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.8f);
            bowSprite.sprite = bowSprites[1];
            yield return new WaitForSeconds(0.4f);
            bowSprite.sprite = bowSprites[2];
            yield return new WaitForSeconds(0.4f);
            bowSprite.sprite = bowSprites[3];
            yield return new WaitForSeconds(0.4f);
            bowSprite.sprite = bowSprites[4];
            yield return new WaitForSeconds(0.4f);

            yield return new WaitUntil(() => enemyStats.protect == false);
        
                Vector2 position = arm.position - (arm.up/ 1.3f);
                GameObject s = projectile;
                s.GetComponentInChildren<DamageObject>().origin = transform;
                GameObject p = Instantiate(s, position, Quaternion.identity);

                p.transform.up = arm.up;
                p.GetComponent<Rigidbody2D>().AddForce(-(p.transform.up * 300));
            
            bowSprite.sprite = bowSprites[0];
        }
    }
    IEnumerator FinishNoLoop()
    {
        yield return new WaitUntil(()=> spriteAnimation.finishedNoLoop == true);
        spriteAnimation.ChangeSpritesLocation(idleSprites, true);
        spriteAnimation.loop = true;
        spriteAnimation.finishedNoLoop = false;
    }
}
