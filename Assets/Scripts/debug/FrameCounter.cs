﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameCounter : MonoBehaviour
{

    public Text frameText;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(countFrames());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator countFrames()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            frameText.text = "F " + (int)(1f / Time.unscaledDeltaTime);
        }
    }
}
