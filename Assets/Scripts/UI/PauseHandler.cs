﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseHandler : MonoBehaviour
{
    public Animator animator;
    public static bool isPaused;
    public GameHandler gameHandler;

    public AudioClip victory;

    public bool isArena;
    
    public Text timer;

    public Timer timerScript;

    AudioHandler audiohandler;
    // Start is called before the first frame update
    void Start()
    {
        GameHandler.inLoading = false;
        Resume();
        gameHandler = GameObject.Find("Gamehandler").GetComponent<GameHandler>();
        audiohandler = GetComponentInChildren<AudioHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !GameHandler.inLoading)
        {
         if(audiohandler) audiohandler.PlayAudio();
            Debug.Log("Pause");
            if (isPaused) Resume();
            else Pause();
        }
    }


    public void Pause()
    {
        GameHandler.isPaused = true;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        isPaused = true;
        animator.SetBool("Pause", true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        GameHandler.isPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isPaused = false;
        animator.SetBool("Pause", false);
        Time.timeScale = 1;
    }
    
    public void Menu()
    {
        Time.timeScale = 1;

        GameHandler.isPaused = false;
        MenuScript.startInChapter = false;
        animator.SetBool("EndChapter", false);
        animator.SetBool("Loading", true);
        StartCoroutine(LoadAsynchronously("Menu", 2));

    }

    public void LevelWin()
    {
        animator.SetBool("Loading", true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ReturnToChapters()
    {
        GameHandler.isPaused = false;
        MenuScript.startInChapter = true;
        Time.timeScale = 1;
        animator.SetBool("Loading", true);
        animator.SetBool("EndChapter", false);
        StartCoroutine(LoadAsynchronously("Menu", 4));
    }

    public void WinGame()
    {
        GameHandler.inLoading = true;
        if (gameHandler.isFinalLevel)
        {
            if(PlayerPrefs.GetInt("Unlocked") < gameHandler.currentLevel.unlockChapter)
            {
                PlayerPrefs.SetInt("Unlocked", gameHandler.currentLevel.unlockChapter);
                PlayerPrefs.Save();
            }

            if (MusicHandler.audioSource)
            {
                MusicHandler.audioSource.clip = victory;
                MusicHandler.audioSource.loop = false;
                MusicHandler.audioSource.Play();
            }
 
            StartCoroutine(WinChapter());
        }
        else
        {
            animator.SetBool("Loading", true);
            StartCoroutine(LoadAsynchronously(gameHandler.currentLevel.nextLevel.sceneName, 2));
        }
    }

    public void Death()
    {
        if (isArena)
        {
            WinGame();
        }
        else
        {
            GameHandler.inLoading = true;
            StartCoroutine(deathDelay());
        }

 
    }

    public void Restart()
    {
        Time.timeScale = 1;
        animator.SetBool("EndChapter", false);
        GameHandler.isPaused = false;
        StartCoroutine(deathDelay());
    }

    IEnumerator deathDelay()
    {
        yield return new WaitForSeconds(2);
        animator.SetBool("Loading", true);
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().name, 2));

    }

    IEnumerator WinChapter()
    {
       if(timerScript) timerScript.stopTimer = true;
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
        float t = GameHandler.playTime;

        int sec = (int)(t % 60);
        t /= 60;
        int minutes = (int)(t % 60);
        t /= 60;
        int hours = (int)(t % 24);
        string playTime = hours + ":" + minutes + ":" + sec;

        if (GameHandler.playTime > PlayerPrefs.GetFloat("Play"))
        {
            PlayerPrefs.SetString("PlayTime", playTime);
            PlayerPrefs.SetFloat("Play", GameHandler.playTime);
        }

       if(timer)timer.text = "You survived for: " + hours + ":" + minutes + ":" + sec;
        animator.SetBool("EndChapter", true);

        GameHandler.isPaused = true;
        Cursor.visible = true;
        isPaused = true;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.Confined;
    }

    IEnumerator LoadAsynchronously(string level, float Delay)
    {

        yield return new WaitForSeconds(Delay);
        yield return new WaitUntil(()=> !GameHandler.isPaused);
        AsyncOperation operation = SceneManager.LoadSceneAsync(level);

       
        while (!operation.isDone)
        {
            Debug.Log(operation.progress);

            yield return null;
        }
    }
}
