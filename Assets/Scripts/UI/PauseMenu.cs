﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class PauseMenu : MonoBehaviour
{

    public static bool paused;

    public MeshRenderer scene;
    public Color pauseColor;

    public Camera renderCam;
    public Vector3 pauseOffset;
    public Vector3 rotationOffset;
    Vector3 normalPos;
    Quaternion normalRotation;
    Vector3 bobOffset;

    public Transform menuCursor;

    bool cooldown;
    bool enableBook;

    UnityEvent[] curOptions;
    public UnityEvent[] pauseEvents;
    public UnityEvent[] deathEvents;
    public UnityEvent[] winEvents;

    public Animator animator;

    public Text rightText;
    List<GameObject> activeObjects;

    public GameObject pauseOptions;

    GameHandler gamehandler;
    // Start is called before the first frame update
    void Start()
    {
        gamehandler = GetComponent<GameHandler>();
        normalPos = renderCam.transform.position;
        normalRotation = renderCam.transform.rotation;
        activeObjects = new List<GameObject>();


        StartCoroutine(EnableBook(0, true));
        StartCoroutine(StartGame(0));
    }

    // Update is called once per frame
    void Update()
    {
     if (!cooldown && !GameHandler.levelEnded) Controls();
        MoveCamera();
        
    }

    void MoveCamera()
    {
        if (enableBook)
        {
            Vector3 desiredPos = new Vector3(normalPos.x + pauseOffset.x + bobOffset.x, normalPos.y + pauseOffset.y + bobOffset.y, normalPos.z + pauseOffset.z);
            Quaternion desiredRotation = Quaternion.Euler(rotationOffset.x, rotationOffset.y, rotationOffset.z);
            renderCam.transform.position = Vector3.Lerp(renderCam.transform.position, desiredPos, 0.05f);
            renderCam.transform.rotation = Quaternion.Lerp(renderCam.transform.rotation, desiredRotation, 0.05f);
        }
        else
        {
            renderCam.transform.position = Vector3.Lerp(renderCam.transform.position, normalPos, 0.05f);
            renderCam.transform.rotation = Quaternion.Lerp(renderCam.transform.rotation, normalRotation, 0.025f);
        }
    }

    void Controls()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (paused) Resume();
            else Pause(true);
        }
    }

    public void SetText(string text)
    {
        rightText.gameObject.SetActive(true);
        rightText.text = text;
        activeObjects.Add(rightText.gameObject);
    }
    
    void DeactivateAll()
    {
        foreach(GameObject g in activeObjects)
        {
            g.SetActive(false);
        }
        activeObjects.Clear();
    }
    public void Resume()
    {
        pauseOptions.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //   animator.SetBool("Paused", false);
        GameHandler.isPaused = false;
        StartCoroutine(StartGame(0));

    }

    public void Pause(bool activateOptions)
    {
        pauseOptions.SetActive(true);

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        GameHandler.isPaused = true;

     //   animator.SetBool("Paused", true);
     
        StartCoroutine(PauseGame(0, 0.02f, false, 0));
    }

    string[] LoadTexts(string first = null, string second = null, string third = null)
    {
        string[] s = new string[3];
        s[0] = first;
        s[1] = second;
        s[2] = third;

        return s;
    }
    
    
    public void Quit()
    {
        Application.Quit();
    }

    public void Death()
    {
        GameHandler.levelEnded = true;
        string[] t = LoadTexts(
            "Restart",
            "Options",
            "Quit"
            );
        
        StartCoroutine(EnableBook(1.6f, true));
        StartCoroutine(PauseGame(1, 0.06f, true, 0));
    }

    public void Win()
    {
        string[] t = LoadTexts(
           "Next Chapter",
           "Options",
           "Quit"
           );
        
        StartCoroutine(EnableBook(1.6f, true));
        StartCoroutine(PauseGame(1, 0.06f, true, 0));
    }
    
    public void Restart()
    {
        Resume();
        SceneManager.LoadScene(gamehandler.currentLevel.sceneName);
    }

    public void NextLevel()
    {
        Resume();
        SceneManager.LoadScene(gamehandler.currentLevel.nextLevel.sceneName);
    }


    IEnumerator PauseGame(float startDelay, float speed, bool Slowdown, float endDelay)
    {
        paused = true;
        cooldown = true;
        yield return new WaitForSeconds(startDelay);
        float amount = 1;
        float highAmount = 0;
        float rangeAmount = 0;

        while (amount > 0)
        {
            yield return new WaitForSeconds(speed);
            if (Slowdown) Time.timeScale = amount;
            amount -= 0.1f;
            highAmount += 0.1f;
            rangeAmount += 0.5f;
            Color color = Color.Lerp(Color.white, pauseColor, highAmount);
            scene.material.SetColor("_ColorTint", color);
          //  scene.material.SetFloat("_Radius", rangeAmount);
            scene.material.SetFloat("_Greyscale", amount);
            scene.material.SetFloat("_Colorscale", highAmount);
        }

        yield return new WaitForSeconds(endDelay);
        Time.timeScale = 0;
        menuCursor.localPosition = Vector2.zero;
        cooldown = false;
    }

    IEnumerator EnableBook(float waitTime, bool value)
    {
        yield return new WaitForSeconds(waitTime);
        enableBook = value;
    }


    IEnumerator StartGame(float startDelay)
    {
        cooldown = true;
        float amount = 0;
        float highAmount = 1;
        float rangeAmount = 5;
        
        scene.material.SetColor("_ColorTint", pauseColor);
        scene.material.SetFloat("_Greyscale", amount);
        scene.material.SetFloat("_Colorscale", highAmount);

        Time.timeScale = 1;
        yield return new WaitForSeconds(0.7f + startDelay);

        StartCoroutine(EnableBook(0, false));

        while (amount < 1)
        {
            yield return new WaitForSeconds(0.02f);
            amount += 0.1f;
            highAmount -= 0.1f;
            rangeAmount -= 0.5f;

            Color color = Color.Lerp(Color.white, pauseColor, highAmount);
            scene.material.SetColor("_ColorTint", color);
           // scene.material.SetFloat("_Radius", rangeAmount);
            scene.material.SetFloat("_Greyscale", amount);
            scene.material.SetFloat("_Colorscale", highAmount);
        }

        yield return new WaitForSeconds(0.4f);
        DeactivateAll();
        paused = false;

        cooldown = false;
    }

    float timer = 0.0f;
    }


