﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICursor : MonoBehaviour
{
    public Option option;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (!PauseMenu.paused) return;
        transform.position = new Vector2(transform.position.x + Input.GetAxis("Horizontal") + Input.GetAxis("Mouse X"), transform.position.y + Input.GetAxis("Vertical") + Input.GetAxis("Mouse Y"));

        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 0.1f);

        if (hit.collider != null)
        {
            if(hit.collider.tag == "Button")
            {
                if(hit.transform.gameObject.GetComponent<Option>() != option)
                {
                   if(option != null) option.text.color = Color.black;
                    option = hit.transform.gameObject.GetComponent<Option>();
                    option.text.color = Color.white;
                }
            }
        }
        else
        {
            if (option != null)
            {
                option.text.color = Color.black;
                option = null;

            }
        }
            if (Input.GetButtonDown("Fire1"))
        {
           if(option != null) option.voidEvent.Invoke();
        }
    }
}
