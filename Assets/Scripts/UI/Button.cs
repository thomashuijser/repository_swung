﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour
{

    public Text[] text;

    public Image[] image;

    public bool isImage;

    [Range(0,1)]
    public float emissionThickness;
    [Range(0,1)]
    public float DissolvePower;

    // Start is called before the first frame update
    void Start()
    {
        if (!isImage)
        {
            text = GetComponentsInChildren<Text>();
            foreach (Text t in text)
            {
                t.material = new Material(t.material);
            }
        }
        else
        {
            image = GetComponentsInChildren<Image>();
            foreach (Image t in image)
            {
                t.material = new Material(t.material);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isImage)
        {
        foreach (Text t in text)
        {
            t.material.SetFloat("_EmissionThickness", emissionThickness);
            t.material.SetFloat("_DissolvePower", DissolvePower);
        }
        }
        else
        {
            foreach (Image t in image)
            {
                t.material.SetFloat("_EmissionThickness", emissionThickness);
                t.material.SetFloat("_DissolvePower", DissolvePower);
            }
        }
    }
}
