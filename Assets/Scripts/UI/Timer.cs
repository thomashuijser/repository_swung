﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    Text text;

    public bool stopTimer;
    // Start is called before the first frame update
    void Start()
    {
        GameHandler.playTime = 0;
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (stopTimer) return;

        float t = GameHandler.playTime;

        int sec = (int)(t % 60);
        t /= 60;
        int minutes = (int)(t % 60);
        t /= 60;
        int hours = (int)(t % 24);

        text.text = hours + ":" + minutes + ":" + sec;
    }
}
