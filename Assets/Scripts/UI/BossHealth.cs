﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour
{

    public Image health;
    EnemyStats enemyStats;
    EnemyPathfinder enemyPathfinder;

    public float maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        enemyStats = GetComponent<EnemyStats>();
        enemyPathfinder = GetComponent<EnemyPathfinder>();
        maxHealth = enemyStats.health;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyPathfinder.target)
        {
            if (!health.transform.parent.gameObject.activeSelf) health.transform.parent.gameObject.SetActive(true);
            SetHealthbar();
        }
        
    }

    void SetHealthbar()
    {
        Vector2 scale = new Vector2(1 / maxHealth * enemyStats.health, health.rectTransform.localScale.y);
        health.rectTransform.localScale = scale;
    }
}
