﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxingCamera : MonoBehaviour
{
    [Range(0,2)]
    public float delay =1;
    public bool xOnly;
    Transform mainCameraPos;
    Vector2 startPos;

    Camera parCamera;
    // Start is called before the first frame update
    void Start()
    {
        
        mainCameraPos = Camera.main.transform;
        startPos = Camera.main.transform.position;
        parCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        SetPosition();
    }

    void SetPosition()
    {
        float x = startPos.x + ((mainCameraPos.position.x - startPos.x) * delay);
        float y = mainCameraPos.position.y;
        if(!xOnly) y = startPos.y + ((mainCameraPos.position.y - startPos.y) * delay);

        transform.position = new Vector2(x,y);
        parCamera.orthographicSize = Camera.main.orthographicSize; 
    }
}
