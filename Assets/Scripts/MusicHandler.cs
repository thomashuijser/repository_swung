﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicHandler : MonoBehaviour
{

    private static MusicHandler instance = null;
    public static bool hasSetMusic;
    public static AudioSource audioSource;

    public static MusicHandler Instance
    {
        get { return instance; }
    }
    // Start is called before the first frame update
    void Awake()
    {

        if (hasSetMusic)
        {
            Destroy(this.gameObject);
            return;
        }
   
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
        DontDestroyOnLoad(this.gameObject);
        hasSetMusic = true;
    }

    // Update is called once per frame
    void Update()
    {
   
    }

    public static IEnumerator SwitchMusic(AudioClip clip = null, bool instantStart = false)
    {

        audioSource.loop = true;
        float startVolume = audioSource.volume;

        if (instantStart)
        {
            audioSource.clip = clip;
            audioSource.Play();
            yield return null;
        }
        else
        {
           

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime;

            yield return null;
        }

        
        audioSource.clip = clip;
        audioSource.Play();

        while (audioSource.volume < startVolume)
        {
            audioSource.volume += startVolume * Time.deltaTime;

            yield return null;
        }
        audioSource.volume = startVolume;
    }

    }
}

