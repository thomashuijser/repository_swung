﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Level", menuName = "Swung/level", order = 1)]
public class Level : ScriptableObject
{
    public Sprite chapterCover;
    [Tooltip("scene name has to be the name of the scene you want to load")]
    public string sceneName;
    [Tooltip("The name of the level shown in the book menu")]
    public string levelName;

    [Tooltip("the story of the level shown in the book menu")]
    public string levelStory;
    [Tooltip("the win story of the level shown in the book menu")]
    public string winStory;
    [Tooltip("the lose story of the level shown in the book menu")]
    public string loseStory;

    public int unlockChapter;

    public Level nextLevel;

    public AudioClip music;

    public bool stopMusic;
}
