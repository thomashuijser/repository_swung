﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    public bool buttonDown;
    public GameObject cursorParticles;
    public GameObject Sword;

    Vector3 centerPoint;
    Vector3 startPosition;
    Vector3 joystickStartPos;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Controller();
    }

   public Vector3 GetMousePosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        return mousePosition;
    }

    void Controller()
    {
        Vector2 position = GetMousePosition();
        if (!buttonDown)
        {
            cursorParticles.transform.position = Vector2.Lerp(cursorParticles.transform.position, position, 0.8f);
        }
        else
        {
            cursorParticles.transform.position = Vector2.Lerp(cursorParticles.transform.position, Sword.transform.position, 0.8f);
        }
    }
}
