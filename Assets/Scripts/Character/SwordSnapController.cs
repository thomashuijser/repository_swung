﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordSnapController : MonoBehaviour
{
    public Transform center;
    public Transform sword;
    Vector2[] snapPositions;
    Vector2 cursorPosition;
    Vector2 curPosition;
    Vector2 desiredPosition;

    [Range(0, 1)]
    public float swordDistance;

    [Range(0, 1)]
    public float snapDistance;

    //needs range!
    public float mouseSensitivity;

    // Start is called before the first frame update
    void Start()
    {
        snapPositions = new Vector2[8];

        cursorPosition = center.position;
        curPosition = center.position;
        desiredPosition = center.position;
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;
        SetSnapPositions();
        MoveCursor();
        Snap();
        MoveSword();
    }

    void SetSnapPositions()
    {
        int angle = 90;
        for (int i = 0; i < snapPositions.Length; i++)
        {
            snapPositions[i] = center.position;
            snapPositions[i] += VectorFromAngle(angle) *  swordDistance;
            angle -= 45;
        }
    }
    void MoveCursor() {

        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        mouseX = Mathf.Clamp(mouseX, -0.1f + curPosition.x, 0.1f + curPosition.y);
        mouseY = Mathf.Clamp(mouseY, -0.1f + curPosition.x, 0.1f + curPosition.y);
        Debug.Log(mouseX + " - " + mouseY);
        mouseX += cursorPosition.x;
        mouseY += cursorPosition.y;
        

        Vector2 position = new Vector2(mouseX, mouseY);
        cursorPosition = position;

        Debug.DrawLine(curPosition, cursorPosition, Color.red);

    }

    void MoveSword()
    {
        sword.position = Vector2.Lerp(sword.position, desiredPosition, 0.125f);
        sword.up = Vector2.Lerp(sword.up, center.position, 0.8f);
    }

    void Snap()
    {
        foreach(Vector2 v in snapPositions)
        {
            Debug.DrawLine(center.position, v, Color.green);
            //sword.transform.position = v;
            if (Vector2.Distance(cursorPosition, v) < snapDistance)
            {
                cursorPosition = v;
                curPosition = v;
                desiredPosition = v;
            }
        }
    }

    Vector2 VectorFromAngle(float theta)
    {
        Vector2 dir = (Quaternion.Euler(0, 0, theta) * Vector2.right);
        return dir;
    }
}
