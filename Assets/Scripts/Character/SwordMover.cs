﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordMover : MonoBehaviour
{
    public Vector2 startPosition;
    public GameObject sword;
    public float swordReach;
    public float velocityStrength;
    public SpriteRenderer chargeSprite;

    CharacterAI cm;
    SpriteRenderer sprite;
    DamageObject damage; 

    float reach;
    float charge;

    Vector2 spritePosition;
    Vector2 velocity;
    Vector2 upvelocity;

    public ParticleSystem particles;

    float heat;
    public Color heatColor;
    Color baseColor;

    public bool enabledController;

    void Start()
    {
        startPosition = sword.transform.localPosition;
        reach = swordReach;
        sprite = sword.GetComponentInChildren<SpriteRenderer>();
        spritePosition = sprite.transform.localPosition;
        cm = GetComponent<CharacterAI>();
        damage = sprite.GetComponent<DamageObject>();

        StartCoroutine(Shine());
        StartCoroutine(ShineTimer());

        baseColor = sprite.material.GetColor("_Color");
    }


    void Update()
    {
        Move();
        Controls();
        HeatSword();
    }

    //moves the sword towards the horizontal and vertical input
    void Move()
    {
        Vector2 desiredPosition;
        Vector2 pos = Vector2.zero;
        if (enabledController)
        {
            float Horizontal = Mathf.Ceil(Input.GetAxis("Horizontal")/2);
            float Vertical = Mathf.Ceil(Input.GetAxis("Vertical")/2);
            // float reach = (swordReach * (Horizontal + Vertical));

            desiredPosition = new Vector2(startPosition.x + ((Input.GetAxisRaw("Horizontal")) * reach), startPosition.y + ((Input.GetAxisRaw("Vertical")) * reach));

        }
        else
        {
            float Xreach = reach - Mathf.Abs(sword.transform.localPosition.y);
            float Yreach = reach - Mathf.Abs(sword.transform.localPosition.x);
            float x = Input.GetAxis("Mouse X")/2f;
            float y = Input.GetAxis("Mouse Y") /2f;

            float horizontal = x + sword.transform.localPosition.x;
            float vertical = y + sword.transform.localPosition.y;
            
            pos = new Vector2(horizontal, vertical);
            
            Vector2 clampPos = Vector2.ClampMagnitude(pos, reach);

            float distance = Vector2.Distance(pos, Vector2.zero);
            desiredPosition = clampPos;
        }

        var direction = (Vector2)sword.transform.localPosition - desiredPosition;
        PullCharacter(pos);
        damage.velocity = -direction * 20;

        Debug.DrawRay(sword.transform.position, -direction * 10);
        sword.transform.localPosition = Vector2.SmoothDamp(sword.transform.localPosition, desiredPosition, ref velocity, 0.05f);

        if(!Input.GetButton("Lock"))sword.transform.up = Vector2.SmoothDamp(sword.transform.up, desiredPosition + (direction * 2), ref upvelocity, 0.01f);

        Vector2 characterKnockback = new Vector2(direction.x, 0);

        //gives the character knockback, looks strange when camera is tracking the player
        //if ((direction.x * 10) + (direction.y * 10) > 2 || (direction.x * 10) + (direction.y * 10) < -2) cm.rb.velocity += -characterKnockback / 4;
    }

    void PullCharacter(Vector2 pos)
    {
        var direction = (Vector2)transform.position - (Vector2)sword.transform.position;
        float x = Mathf.Abs(pos.x);
        float y = Mathf.Abs(pos.y);
        if(x + y > reach*2)
        {
            cm.rb.velocity = -(direction * 5);
        }
    }
    
    //checks if character is sending the axis inputs
    bool IsUsingControls()
    {
        return (Input.GetAxis("Horizontal") + Input.GetAxis("Vertical") != 0);
    }

    //player controls
    void Controls()
    {
    //    if (!IsUsingControls()) return;

        if (Input.GetButton("Fire1"))
        {
            reach = swordReach / 2;
            Shake();
            if (heat <= 100)
            {
                charge += 100 * Time.deltaTime;
                heat += 100 * Time.deltaTime;
                chargeSprite.material.SetFloat("_DissolvePower", heat / 100);
            }
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            if(charge > 20) LaunchCharacter(charge/18);
            charge = 0;
            reach = swordReach;
        }
    }


    //gives the character velocity towards the swords position
    void LaunchCharacter(float force)
    {
        cm.rb.velocity = (sword.transform.up * force * 1.4f);
        particles.Play();
        
    }

    void HeatSword()
    {
        if (Input.GetButton("Fire1")) return;

        if (heat > 0)
        {
            heat -= 60 * Time.deltaTime;
            chargeSprite.material.SetFloat("_DissolvePower", heat / 100);
        }
        else heat = 0;
        
    }

    //shakes the sword
    void Shake()
    {
        Vector2 shakeposition = new Vector2(spritePosition.x + Random.insideUnitSphere.x * 0.04f * 2, spritePosition.y + Random.insideUnitSphere.x * 0.01f);
        sprite.transform.localPosition = Vector2.Lerp(sprite.transform.localPosition, shakeposition, 0.5f);
    }

    IEnumerator Shine()
    {
        float time = 0;

        while (time < 1)
        {
            yield return new WaitForSeconds(0.01f);
            time += 0.03f;
           sprite.material.SetFloat("_ShineLocation", time);
        }
        while (time > 0)
        {
            yield return new WaitForSeconds(0.01f);
            time -= 0.03f;
            sprite.material.SetFloat("_ShineLocation", time);
        }
    }


    //temporary needs to be removed later
    IEnumerator ShineTimer()
    {
        while (true)
        {
            float time = Random.Range(5, 10);
            yield return new WaitForSeconds(time);
            StartCoroutine(Shine());
        }
    }
}
