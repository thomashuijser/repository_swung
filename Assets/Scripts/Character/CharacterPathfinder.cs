﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPathfinder : MonoBehaviour
{
    public float maxRaycastDistance;
    public LayerMask layermask;
    public LayerMask enemyMask;

    public float[] directionsValue;
    public string[] directionHitTag;
    public Transform[] directionHitTransform;
 
    public static List<EnemyStats> spottedEnemies;

    public bool nearbyEnemies;

    // Start is called before the first frame update
    void Start()
    {

        spottedEnemies = new List<EnemyStats>();
        directionHitTag = new string[8];
        directionsValue = new float[8];
        directionHitTransform = new Transform[8];
    }

    // Update is called once per frame
    void Update()
    {
        Pathfinder();
        CircleCast();
    }

    void CircleCast()
    {
       RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 4, transform.forward, 2, enemyMask);

        if(hit.Length > 0)
        {
            nearbyEnemies = true;
        }
        else
        {
            nearbyEnemies = false;
        }
    }

    void Pathfinder()
    {
            int angle = 90;

            for (int i = 0; i < directionsValue.Length; i++)
            {
            Raycasting(VectorFromAngle(angle), transform.position, i, true);
             angle -= 45;
            }
    }

    public float RaycastDirection(int angle, Vector2 position)
    {
       return Raycasting(VectorFromAngle(angle), position, 0, false);
    }

    public float Raycasting(Vector2 direction, Vector3 position, int i, bool registerCast)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, direction, maxRaycastDistance, layermask);

        if (hit.collider != null)
        {
            if (registerCast)
            {
                directionsValue[i] = hit.distance;
                directionHitTag[i] = hit.collider.tag;
                directionHitTransform[i] = hit.transform;
            }

            if (hit.distance <= 1) Debug.DrawRay(position, direction * hit.distance, Color.green);
            else Debug.DrawRay(position, direction * hit.distance, Color.yellow);

            if (hit.transform.tag == "Enemy")
            {
                hit.transform.GetComponent<EnemyPathfinder>().target = gameObject.transform;
                EnemyStats e = hit.transform.GetComponent<EnemyStats>();
                if (!spottedEnemies.Contains(e))
                spottedEnemies.Add(e);
            }
            return hit.distance;
        }
        else
        {
            if (registerCast)
            {
                directionsValue[i] = maxRaycastDistance;
                directionHitTag[i] = "null";
                directionHitTransform[i] = null;
            }
            Debug.DrawRay(position, direction * 10, Color.red);
        }
        return maxRaycastDistance * 10;
    }

    Vector2 VectorFromAngle(float theta)
    {
        Vector2 dir = (Quaternion.Euler(0, 0, theta) * Vector2.right);
        return dir;
    }
}
