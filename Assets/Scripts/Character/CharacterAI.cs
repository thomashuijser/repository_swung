﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Directions { Left = -1, Still = 0, Right = 1 }

public class CharacterAI : MonoBehaviour
{
    CharacterPathfinder characterPathfinder;

    [HideInInspector]
    public SpriteAnimation spriteAnimation;
    SwordMover swordMover;

    [HideInInspector]
    public Rigidbody2D rb;
    Vector2 velocity;

    [HideInInspector]
    public bool isMoving;
    bool checkSurroundings;
    
    public bool isGrounded;

    public bool isUnconscious;
    public bool scaredHight;

    public bool lockInCamera;

    public Directions direction = Directions.Right;
    public float movementSpeed;
    public Transform swordRotationPoint;
    public Transform sword;

    public Sprite[] freezeSprites;
    public Sprite[] walkSprites;
    public Sprite[] idleSprites;
    public Sprite[] unconsciousSprites;
    public Sprite[] jumpingSprites;

    public SpriteRenderer headSprite;
    public Transform arm;

    // Start is called before the first frame update
    void Start()
    {
        isMoving = true;
        rb = GetComponent<Rigidbody2D>();
        spriteAnimation = GetComponentInChildren<SpriteAnimation>();
        characterPathfinder = GetComponent<CharacterPathfinder>();
        swordMover = GetComponent<SwordMover>();
        checkSurroundings = true;

        headPos = headSprite.transform.localPosition;
        armorPos = spriteAnimation.spriteRenderer.transform.localPosition;

        StartCoroutine(SetUnconscious(1, null));

        isGrounded = true;

        Flip();
    }

    // Update is called once per frame
    void Update()
    {

        return;

        SetScared();
        SetArmPosition();

        if (checkSurroundings) CheckSurroundings();
        if (isUnconscious || scaredHight)
        {
            Shake(headSprite.transform, headPos, 0.05f);
            Shake(spriteAnimation.spriteRenderer.transform, armorPos, 0.05f);
            return;
        }
        if (isMoving) Move();
        if (characterPathfinder.directionsValue[4] > 0.7f)
        {
            isGrounded = false;
        }
    }

    void SetArmPosition()
    {
        Vector2 position = new Vector2(swordRotationPoint.position.x + sword.position.x, swordRotationPoint.position.y + sword.position.y)/2;
        arm.position = position;
        arm.transform.up = (arm.transform.position - sword.transform.position).normalized;
    }

    void Move()
    {

        Vector2 desiredPosition = new Vector2(transform.position.x + (int)direction * movementSpeed, transform.position.y);

        if (lockInCamera)
        {
            rb.position = Vector2.SmoothDamp(rb.position, CameraScript.ClampObject(desiredPosition), ref velocity, 0.2f);
        }
        else
        {
            rb.position = Vector2.SmoothDamp(rb.position, desiredPosition, ref velocity, 0.2f);
        }
    }

    void SetMoving(bool value)
    {
        isMoving = value;
        if (value) spriteAnimation.ChangeSprites(walkSprites, true);
        else spriteAnimation.ChangeSprites(freezeSprites, true);
    }

    void CheckSurroundings()
    {
        Vector2 pos;

        pos = new Vector2(transform.position.x + (int)direction, transform.position.y);
        
        if (isGrounded)
        {
            //checks if there is a tile infront of the character, if not he freezes
            if (characterPathfinder.RaycastDirection(90 - (CalculateRayDirection(4) * 45), pos) > 2)
            {
                if (!scaredHight) scaredHight = true;
            }
            else scaredHight = false;
        }

        //no need to check pathfindings if the character is frozen....
        if (isUnconscious) return;

        if (characterPathfinder.directionsValue[CalculateRayDirection(2)] < 0.8f)
        {  
            if (characterPathfinder.directionHitTag[CalculateRayDirection(2)] == "Tile")
            {
                 pos = new Vector2(transform.position.x, transform.position.y + 1);

                int angle = 90 - (CalculateRayDirection(2) * 45);
                if (characterPathfinder.RaycastDirection(angle, pos) > 0.8f)
                {
                    ReactionJump(6);
                }
                else
                {
                    Flip();
                }
            }
        }
        }


    Vector2 headPos;
    Vector2 armorPos;
    //shakes a sprite
    void Shake(Transform t, Vector2 p, float amount)
    {
        Vector2 shakeposition = new Vector2(p.x + Random.insideUnitSphere.x * amount, p.y + Random.insideUnitSphere.x * (amount/4));
        t.localPosition = Vector2.Lerp(t.localPosition, shakeposition, 0.5f);
    }

    void SetScared()
    {
        if (CharacterPathfinder.spottedEnemies.Count > 0 || scaredHight)
        {
            if (!isUnconscious)
            {
                headSprite.enabled = false;
                isUnconscious = true;
                spriteAnimation.ChangeSprites(freezeSprites, true);
                spriteAnimation.PauseAnimation(true, true);
                swordRotationPoint.localPosition = new Vector2(swordRotationPoint.localPosition.x, 0.02f);

            }
        }
        else if (isUnconscious)
        {
            headSprite.enabled = true;
            isUnconscious = false;
            spriteAnimation.ChangeSprites(walkSprites, true);
            spriteAnimation.PauseAnimation(false, true);
            swordRotationPoint.localPosition = new Vector2(swordRotationPoint.localPosition.x, 0.04f);
        }
    }

    void ReactionJump(float force)
    {
        if (!isGrounded) return;
        rb.velocity = transform.up * force;//new Vector2(force / 4, force * (int)direction);
       // spriteAnimation.ChangeSprites(jumpingSprites, true);
        isGrounded = false;
    }

    //flips the character
    public void Flip()
    {
        spriteAnimation.spriteRenderer.flipX = !spriteAnimation.spriteRenderer.flipX;
        headSprite.flipX = !headSprite.flipX;

        if (spriteAnimation.spriteRenderer.flipX)
        {
            direction = Directions.Right;
            swordRotationPoint.localPosition = new Vector2(0.244f, swordRotationPoint.localPosition.y);
        }
        else
        {
            direction = Directions.Left;
            swordRotationPoint.localPosition = new Vector2(-0.244f, swordRotationPoint.localPosition.y);
        }
    }


    //sets the needed delay
    void SetDelay(float d)
    {
        delay = d;
        curDelay = delay;
    }

    float delay = 0;
    float curDelay = 0;

    //delay countdown
    bool Delay()
    {
        curDelay -= Time.deltaTime;
        if (curDelay <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    int CalculateRayDirection(int rayDirection)
    {
        if (direction == Directions.Right) return rayDirection;
        else return 8 - rayDirection;
    }
    int CalculateRayAngle(int rayDirection)
    {
        if (direction == Directions.Right) return rayDirection;
        else return 8 - rayDirection;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(characterPathfinder.directionsValue[4] < 0.7f)
        {
            isGrounded = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    { 
        

    }

    IEnumerator SetUnconscious(float time, Sprite[] sprites)
    {
        isUnconscious = true;
        if(sprites != null) spriteAnimation.ChangeSprites(sprites, true);
        yield return new WaitForSeconds(time);
        isUnconscious = false;
    }
}
