﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum Interests { love, like, Scared }
public class CharacterScript : MonoBehaviour
{
    
    [Tooltip("The characters head")]
    public SpriteRenderer headSprite;
    [Tooltip("The characters body")]
    public SpriteRenderer bodySprite;

    public SpriteRenderer armSprite;

    public Transform pullArm;

    public GameObject sword;

    [HideInInspector]
    public DistanceJoint2D distanceJoint;

    [HideInInspector]
    public Rigidbody2D rigidbody;

    [HideInInspector]
    public SwordScript swordScript;
    CharacterPathfinder characterPathfinder;
    Controls controls;
    public SpriteAnimation spriteAnimation;

    [HideInInspector]
    public Transform interest;

    [HideInInspector]
    public LineRenderer armLine;
    bool isHanging;


    public bool isMoving;
    public bool isGrounded;
    public bool lockCharacter;
    public bool canMove = true;

    public Transform target;

    [HideInInspector]
    public float directionValue;
    public float maxSpeed;
    float curSpeed;

    float posDifference;

    public Vector2 armOffset;

    [HideInInspector]
    public Vector2 curArmOffset;
    public Transform swordOffset;

    public AudioSource voiceSource;
    public AudioClip fear;
    public AudioClip hurt;
    public AudioClip pride;
    public AudioClip surprise;
    public AudioClip think;

    public AudioSource footStepSource;
    public AudioClip leftFootstep;
    public AudioClip rightFootstep;

    public CapsuleCollider2D characterCollider;

    public PhysicsMaterial2D flyingPhysics;
    public PhysicsMaterial2D walkingPhysics;


    private void Awake()
    {
    
        GameHandler.playerTransform = transform;
    }
    // Start is called before the first frame update
    void Start()
    {
        target = sword.transform;
        armLine = GetComponentInChildren<LineRenderer>();
        controls = GetComponent<Controls>();
        swordScript = sword.GetComponent<SwordScript>();
        spriteAnimation = GetComponentInChildren<SpriteAnimation>();
        characterPathfinder = GetComponent<CharacterPathfinder>();
        rigidbody = GetComponent<Rigidbody2D>();
        interest = sword.transform;
        distanceJoint = GetComponent<DistanceJoint2D>();
        curArmOffset = armOffset;

        StartCoroutine(FootstepCicle());

        PlayVoice(pride);
    }

    // Update is called once per frame
    void Update()
    {
        if (lockCharacter) return;
        if (!swordScript.isPulling) SetTarget();
        else target = sword.transform;

        distanceJoint.anchor = curArmOffset;

        isGrounded = controls.grounded;
       // Checkgrounded();
        RotateToInterest();
        curSpeed = maxSpeed;
        SetDirection();

        SetPullArm();

        if (swordScript.isPulling) SetPullArm();

        if (!isGrounded)
        {
      
            if(characterCollider.sharedMaterial == walkingPhysics)
            {
              //  controls.grounded = false;
                characterCollider.enabled = false;
                characterCollider.sharedMaterial = flyingPhysics;
                characterCollider.enabled = true;
            }

        }
        else 
        {
            transform.up = Vector2.zero;
            if (characterCollider.sharedMaterial == flyingPhysics)
            {
                Debug.Log("Switch");
             //   controls.grounded = true;
                characterCollider.enabled = false;
                characterCollider.sharedMaterial = walkingPhysics;
                characterCollider.enabled = true;
            }
        }
    }

    private void LateUpdate()
    {
        if (isGrounded)
        {
          //  Movement();
        }
    }

    void Movement()
    {
        Vector2 velocity = rigidbody.velocity;
        
        velocity.x = 1.3f;

        rigidbody.velocity = velocity;
    }

    void RotateTowardsSword()
    {
        Vector3 targetDirection = sword.transform.position - bodySprite.transform.position;

        float x = Mathf.Clamp(targetDirection.x, -0.1f, 0.1f);
        float y = Mathf.Clamp(targetDirection.y, -0.1f, 0.1f);

        targetDirection = new Vector2(x ,y);

        Debug.Log(targetDirection);
        transform.up = targetDirection;
    }

    //sets the arms position and rotation when its being pulled
    void SetPullArm()
    {
        armLine.SetPosition(0, (Vector2)transform.position + curArmOffset);
        armLine.SetPosition(1, (Vector2)swordOffset.position);
    }

    void SetTarget()
    {
        bool spottedTarget = false;
        foreach(Transform t in characterPathfinder.directionHitTransform)
        {
            if(t != null)
            if(t.tag == "Mirror")
            {
                spottedTarget = true;
                target = t;
              }
        }
        if (!spottedTarget)
        {
            target = sword.transform;
        }
    }

    //needs fixing :(
    void RotateToInterest()
    {
        float speed = 10;

        float clampValue = 1;
        Vector2 pos = headSprite.transform.position;
        float yMax = pos.y + clampValue;
        float yMin = pos.y - clampValue;

        float xMax = pos.x + clampValue;
        float xMin = pos.x - clampValue;

        Vector2 lookPos = new Vector2(0 - headSprite.transform.position.x, Mathf.Clamp((interest.position.y * 1.5f) - headSprite.transform.position.y, -1, 1));

        Vector2 direction = lookPos;

        Quaternion lookRotation = Quaternion.LookRotation(direction);

        float Ypos = Mathf.Clamp(sword.transform.position.y - headSprite.transform.position.y, -1, 1);

        float angle = Vector2.Angle(headSprite.transform.position, headSprite.transform.position);

        lookRotation = (bodySprite.flipX) ? Quaternion.Euler(0, 0, -lookRotation.eulerAngles.x)
                                           : Quaternion.Euler(0, 0, lookRotation.eulerAngles.x);

      //  headSprite.transform.rotation = Quaternion.Slerp(headSprite.transform.rotation, lookRotation, speed * Time.deltaTime);
        
        //340 20
        //flips the character
         posDifference = target.position.x - transform.position.x;

        
        if(posDifference < -0.1f)
        {
            if (bodySprite.flipX)
            {
                Flip(false);
                directionValue = -1;
            }
        }
        else
        {
            if (!bodySprite.flipX)
            {
                Flip(true);
                directionValue = 1;
            }
        }
    }
    
    //checks if the character is near interest and if he can move
    void SetDirection()
    {
         if(Mathf.Abs(posDifference) < 1)
        {
           canMove = false;
        }else if(!canMove && Mathf.Abs(posDifference) > 1.2f)
        {
            canMove = true;
        }
    }

    void Flip(bool flipSide)
    {
     //  if(isGrounded) rigidbody.AddForce(transform.up * 100);

        curArmOffset = (flipSide)
            ? new Vector2(-armOffset.x, armOffset.y)
            : new Vector2(armOffset.x, armOffset.y);

        headSprite.flipX = flipSide;
        bodySprite.flipX = flipSide;
        armSprite.flipX = flipSide;
    }

    void Checkgrounded()
    {
        if (!isGrounded)
        {
            if (characterPathfinder.directionHitTag[4] == "Tile" && characterPathfinder.directionsValue[4] < 0.51f
                || characterPathfinder.directionHitTag[4] == "FatBird" && characterPathfinder.directionsValue[4] < 0.51f)
            {
                isGrounded = true;
            }
        }
        else
        {
            if (characterPathfinder.directionHitTag[4] == "Tile" && characterPathfinder.directionsValue[4] > 0.51f
                || characterPathfinder.directionHitTag[4] == "FatBird" && characterPathfinder.directionsValue[4] > 0.51f)
            {
                isGrounded = false;
            }
        }
    }

    public void PlayVoice(AudioClip clip)
    {
        if (voiceSource.isPlaying) return;
        voiceSource.clip = clip;
        voiceSource.Play();
    }

    IEnumerator FootstepCicle()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (canMove && controls.grounded && !controls.isScared)
            {
                if (footStepSource.clip = leftFootstep) footStepSource.clip = rightFootstep;
                else footStepSource.clip = leftFootstep;
                footStepSource.Play();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "FatBird")
        {
            transform.parent = collision.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "FatBird")
        {
            transform.parent = null;
        }
    }
}
