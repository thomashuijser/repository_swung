﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : Movement
{

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    CharacterPathfinder characterPathfinder;
    SpriteAnimation spriteAnimation;

    public float direction;

    [HideInInspector]
    public bool isScared;

    float swordDistance;

    //  private SpriteRenderer spriteRenderer;
    // private Animator animator;

    // Use this for initialization
    void Awake()
    {
        characterPathfinder = GetComponent<CharacterPathfinder>();
        spriteAnimation = GetComponentInChildren<SpriteAnimation>();
        StartCoroutine(ScaredTime());
        //    spriteRenderer = GetComponent<SpriteRenderer>();
        //     animator = GetComponent<Animator>();
    }


    protected override void ComputeVelocity()
    {

        swordDistance = Vector2.Distance(swordScript.transform.position, transform.position);

        GetSpottedEnemies();
        Vector2 move = Vector2.zero;

        float x = characterScript.directionValue;

        if (isScared)
        {
            characterScript.canMove = false;
        }

        if (characterPathfinder.directionsValue[CalculateSide(2)] < 0.7f && characterPathfinder.directionHitTag[CalculateSide(2)] == "Tile" 
            && grounded)
        {
            Vector2 pos = new Vector2(transform.position.x, transform.position.y + 1);

            if (characterPathfinder.RaycastDirection(90 - (CalculateSide(2) * 45), pos) > 0.7f && swordDistance > 1.2f)
            {
               if(!isScared) velocity.y = jumpTakeOffSpeed;
            }
        }
        else if(characterPathfinder.directionsValue[CalculateSide(3)] > 0.7f)
        {
            Vector2 pos = new Vector2(transform.position.x + characterScript.directionValue, transform.position.y);

            if (characterPathfinder.RaycastDirection(90 - (4 * 45), pos) > 2f)
            {
                x = 0;
                characterScript.canMove = false;
                isScared = true;
                addScaredTime();
            }
        }

        if (characterScript.canMove)
            move.x = x;
        else move.x = 0;
        
        //if (Input.GetButtonDown("Jump") && grounded)
        //{
        //    velocity.y = jumpTakeOffSpeed;
        //}
        //else if (Input.GetButtonUp("Jump"))
        //{
        //    if (velocity.y > 0)
        //    {
        //        velocity.y = velocity.y * 0.5f;
        //    }
        //}

        //    bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        //      if (flipSprite)
        //      {
        //      spriteRenderer.flipX = !spriteRenderer.flipX;
        //     }

        //   animator.SetBool("grounded", grounded);
        //   animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

    int CalculateSide(int side)
    {
        if(characterScript.directionValue == 1)
        {
            return side;
        }
        else
        {
            return 8 - side;
        }
    }


    int scareTime;
    public void addScaredTime()
    {
        scareTime = Mathf.Clamp(scareTime + 1, 0, 2);

        if (swordScript.isPulling) scareTime = 0;
             }


    void GetSpottedEnemies()
    {
        foreach(string s in characterPathfinder.directionHitTag)
        {
            if(s == "Enemy")
            {
                addScaredTime();
            }
        }

        if(characterPathfinder.directionHitTag[CalculateSide(3)] == "Spikes"
            || characterPathfinder.directionHitTag[CalculateSide(2)] == "Spikes"
            || characterPathfinder.directionHitTag[CalculateSide(1)] == "Spikes"
          ||  characterPathfinder.directionHitTag[CalculateSide(3)] == "FatBird"
            || characterPathfinder.directionHitTag[CalculateSide(2)] == "FatBird"
            || characterPathfinder.directionHitTag[CalculateSide(1)] == "FatBird"
            || characterPathfinder.nearbyEnemies)
        {
            addScaredTime();
        }

        //if (CharacterPathfinder.spottedEnemies.Count > 0)
        //{
        //    isScared = true;
        //}
        //else
        //{
        //    isScared = false;
        //}
    }

    IEnumerator ScaredTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            
                if(scareTime > 0)
            {
                scareTime--;
                isScared = true;
            }
            else
            {
                isScared = false;
            }
        }
    }
}