﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeRotation : MonoBehaviour
{
    public Transform eye;
    public Transform pupil;

    [HideInInspector]
    public Transform target;

    Vector2 centerPos;
    // Start is called before the first frame update
    void Start()
    {
        centerPos = pupil.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        MovetoTarget();
    }

    void MovetoTarget()
    {
        if(target != null)
        {
            Vector2 direction = target.position - eye.position;
            direction = new Vector2(Mathf.Clamp(direction.x, -2, 2), Mathf.Clamp(direction.y, -2, 2));

            Vector2 position = (Vector2)eye.position + (direction /100);
            pupil.position = position;
        }
    }
}
