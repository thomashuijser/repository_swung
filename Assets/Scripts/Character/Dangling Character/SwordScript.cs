﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwordScript : MonoBehaviour
{
    public Vector2 centerOffset;
    public float reach;
    public Transform character;
    DamageObject damageObject;
    public GameObject swordChild;

    [Range(0,100)]
    public float stamina = 100;

    float speed;
    float distance;

    public DistanceJoint2D distanceJoint2D;
    Rigidbody2D rigidbody;

    public SpriteRenderer sprite;

    CharacterScript characterScript;
    Vector2 spritePosition;
    Vector2 rotatePosition;
    Vector2 rotation;
    Vector2 center;

    public CameraScript cameraScript;

    public Sprite closedEye;
    public Sprite openEye;

    [Range(0,1)]
    public float mouseSensitivity;

    [HideInInspector]
    public bool isPulling;

    float verticalSpeed;
    float horizontalSpeed;
    public float gravity;

    bool isMoving;
    bool inAttack;
    bool inCooldown;
    bool inInteraction;

    public AudioSource audioSource;
    public AudioClip swooshAudio;
    public AudioClip pullAudio;

    [Range(0,100)]
    public float gribStrength = 100;

    EyeRotation eyeRotation;

    public Image staminaBar;

    public LayerMask circleMask;

    string targetTag;
    GameObject target;
    GameObject moveableObject;

    public Collider2D childCollider;

    public ParticleSystem particles;
    
    
    // Start is called before the first frame update
    void Start()
    {

        mouseSensitivity = PlayerPrefs.GetFloat("Sensitivity") + 0.1f;
        desiredPos = transform.position;
        SetStamina();
        rigidbody = GetComponent<Rigidbody2D>();
        damageObject = GetComponentInChildren<DamageObject>();
        spritePosition = sprite.transform.localPosition;
        characterScript = character.GetComponent<CharacterScript>();

        eyeRotation = GetComponentInChildren<EyeRotation>();

        up = transform.up;

        eyeRotation.target = character;

        GameHandler.swordTransform = transform;
    }

    void SetStamina()
    {
        gribStrength = Mathf.Clamp(gribStrength, 0 , 100);
        staminaBar.transform.localScale = new Vector2(gribStrength/ 100, 1);
    }


    private void FixedUpdate()
    {
        //  Movement(); // old version
        //Rotate();

        MouseMovement();

        if (!isPulling && Input.GetButton("Fire2"))
        {
            RotateSword();
            mouseSpeed = 0.25f;
        }

        else
         {
            mouseSpeed = 1;
           // KeyMovement();
            damageObject.damage = 0;
            damageObject.velocity = Vector2.zero;
        }
    }


    Vector2 desiredPos;
    Vector2 curSpeed;
    float mouseSpeed = 1;

    float yCountUp;

    void MouseMovement()
    {
        float maxSpeed = 1f;

        float lerpSpeed = 0.2f;
        float x = (Mathf.Clamp(Input.GetAxis("Mouse X")/4, -maxSpeed, maxSpeed)) * mouseSensitivity * mouseSpeed;
        float y = (Mathf.Clamp(Input.GetAxis("Mouse Y")/4,-maxSpeed, maxSpeed)) * mouseSensitivity * mouseSpeed;

        Vector2 pos;



        if(gribStrength <= 0)
        {
            y = Mathf.Clamp(y, -2, 0);
        }

        if(isPulling) lerpSpeed = 0.5f;


        yCountUp += y;
        yCountUp = Mathf.Clamp(yCountUp, 0, 1);
        if (isPulling && !characterScript.isGrounded && yCountUp <= 0)
        {
            pos = new Vector2(desiredPos.x + x, transform.position.y);
        }
        else
        {
            pos = new Vector2(desiredPos.x + x, desiredPos.y + y);
        }



        Debug.Log(yCountUp);

        pos = GameHandler.Cam.WorldToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        pos = GameHandler.Cam.ViewportToWorldPoint(pos);

        desiredPos = pos;

        Vector2 startpos = rigidbody.position;

        Vector2 nextPos = Vector2.SmoothDamp(transform.position, pos, ref curSpeed, lerpSpeed);
        float usedStamina = nextPos.y - rigidbody.position.y;
        yCountUp -= usedStamina;
        rigidbody.MovePosition(nextPos);


        //reduces the stamina
        if (usedStamina > 0 && isPulling)
        {
            SetGrib((10 * usedStamina) * 0.9f);
            particles.Play();
            shakeStrength = 1;
        }
        else
        {
            shakeStrength = 0.1f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SphereCaster();

     

        Controls();

        if(characterScript.isGrounded && !isPulling) SetGrib(15f * Time.deltaTime);
    }
    
    Vector2 upVelocity;
    Vector2 velocity;

    void Idle()
    {
        transform.localPosition = new Vector2(transform.position.x, transform.position.y +(0.2f + (Mathf.Sin(Time.deltaTime)/10)));
    }
    
    Vector2 up;
    
    void SetDesiredPosition()
    {
        
    }

    void Movement()
    {
        var center = (Vector2)character.position + centerOffset;
        float x = (Input.GetAxis("Horizontal")/2);
        float y = (gribStrength > 0)? (Input.GetAxis("Vertical")/2)
                : 0;

        if(isPulling && y > 0)
        {
            particles.Play();
           // SetGrib(y);
        }

        if (x + y != 0)
        {
            isMoving = true;
        }
        else isMoving = false;

        Vector2 desiredDirection = new Vector2(x, y);
        Vector2 desiredPosition = (Vector2)transform.position + (desiredDirection * 6 * Time.fixedDeltaTime);
       
       // Vector2 desiredPosition = new Vector2(transform.position.x + (x * 0.15f), transform.position.y + (y * 0.1f));
        Vector2 direction = (Vector2)transform.position - desiredPosition;

      //  rotation = ((desiredPosition - (Vector2)transform.position) * 2) + (up /2);

        if (isPulling)
        {
            rotation = ((Vector2)transform.position - center) + up;
            swordChild.transform.up = transform.up;
        }
        Rotate();

        rigidbody.MovePosition(desiredPosition);
    }


    //move to gamehandler <------- !!!!!
    public static Vector3 ClampMagnitude(Vector3 v, float max, float min)
    {
        double sm = v.sqrMagnitude;
        if (sm > (double)max * (double)max) return v.normalized * max;
        else if (sm < (double)min * (double)min) return v.normalized * min;
        return v;
    }

    void getNearbyObjects()
    {

    }

    //Rotate the sword
    void RotateSword()
    {
        float x = Input.GetAxis("Mouse X")  * Time.fixedDeltaTime * 1.1f;
        float y = (Input.GetAxis("Mouse Y")  * Time.fixedDeltaTime * 1.1f);

        float xy = x + y;
        
        if(xy != 0)
        {
            damageObject.DamageMulti = 1;
        }
        else
        {
            damageObject.DamageMulti = 0;
        }
 

        if (Mathf.Abs(xy) > 0.09f)
        {
            PlayAudio(swooshAudio, false);
        }
        else
        {
         //   audioSource.Stop();
        }
        
        float maxMouseSpeed = 0.05f;
        
        x = Mathf.Clamp(x, -maxMouseSpeed, maxMouseSpeed);
        y = Mathf.Clamp(y, -maxMouseSpeed, maxMouseSpeed);

        Vector2 des = new Vector2(x,y);
        Vector2 desiredPos = rotatePosition + (des);
      //  Vector2 desiredPos = new Vector2(rotatePosition.x + x, rotatePosition.y + y);

        desiredPos = Vector2.ClampMagnitude(desiredPos, 0.2f);
        rotatePosition = desiredPos;

        Vector2 direction = new Vector2(Mathf.Clamp(x,-1f,1f),Mathf.Clamp(y,-1f,1f));
        swordChild.transform.localPosition = desiredPos;
        

        if(xy != 0)
        {
            damageObject.damage = 1;
            damageObject.velocity = direction;
        }
        else
        {
            damageObject.damage = 0;
            damageObject.velocity = Vector2.zero;
        }
     

        Debug.DrawLine(transform.position, (Vector2)swordChild.transform.position + direction * 10);
        swordChild.transform.up = Vector2.Lerp(swordChild.transform.up, rotatePosition, 0.85f);
    }

    void Rotate()
    {
        transform.up = Vector2.Lerp(transform.up, rotation, 0.5f);
    }

    void Controls()
    {
        PullCharacter();

        //if (Input.GetButtonDown("Dash"))
        //{
        //    Dash();
        //}
    }

    void SphereCaster()
    {
        RaycastHit2D hit;

        hit = Physics2D.CircleCast(transform.position, 1, transform.forward, 1, circleMask);

        if (hit)
        {
            target = hit.transform.gameObject;
            eyeRotation.target = hit.transform;
            targetTag = hit.transform.tag;
            if(hit.transform.tag == "Player" && !Input.GetButton("Fire1") || hit.transform.tag == "Interactable" && !Input.GetButton("Fire1"))
            {
                Vector2 pos = new Vector2(hit.transform.position.x, hit.transform.position.y + 1);
                GameHandler.ActivateKey(true);
                GameHandler.SetKeyPosition(pos);
            }
            else
            {
                GameHandler.ActivateKey(false);
            }

        }
        else
        {
            target = null;
            eyeRotation.target = null;
            GameHandler.ActivateKey(false);

        }
    }


    private Vector3 zAxis = new Vector3(0, 0, 1);
    void KeyMovement()
    {
        float value = Input.GetAxis("Rotate");

        damageObject.damage = (int)Mathf.Abs(value);

        swordChild.transform.RotateAround(transform.position, zAxis, value * 800 * Time.deltaTime);
    }

    void SetOutline(SpriteRenderer[] spriteRenderer, Color color)
    {
       foreach(SpriteRenderer s in spriteRenderer)
        {
            s.material.SetColor("_Color", color);
        }
    }

    void MoveObject(GameObject o)
    {
        o.GetComponent<MoveableObject>().PickUp(swordChild.transform);
    }

    void ReleaseObject(GameObject o)
    {
        o.GetComponent<MoveableObject>().Release();
    }

    void PullCharacter()
    {
        
            if (Input.GetButtonDown("Fire1"))
            {
            if (inInteraction) return;

            if (!inCooldown && targetTag == "Player" && !cameraScript.inChokepoint)
            {
                cameraScript.SetCameraZoom(true, 3.5f);
                PlayAudio(pullAudio, true);
            }
            else if (targetTag == "Interactable")
            {
                MoveObject(target);
                moveableObject = target;
                childCollider.enabled = true;
                inInteraction = true;
            }
            }

            if (Input.GetButton("Fire1"))
            {
            if (inInteraction) return;
            if (!inCooldown && eyeRotation.target != null && targetTag == "Player")
            {
                if(eyeRotation.target.tag == "Player")
                {
                    isPulling = true;
                    reach = 0.6f;
                    distanceJoint2D.enabled = true;
                    Shake();
                    speed = 0.2f;
                    sprite.sprite = closedEye;
                }
            }
        }
        
         if (distanceJoint2D.enabled == true && !Input.GetButton("Fire1") || inCooldown)
        {
            if (cameraScript.overrideScreenZoom)
            {
                cameraScript.SetCameraZoom(false);
            }

            rigidbody.velocity = Vector2.zero;
            isPulling = false;
            reach = 0.4f;
            speed = 1.2f;
            distanceJoint2D.enabled = false;
            sprite.sprite = openEye;

            //makes sure the character doesn't go flying off
            float maxSpeed = 3;
            Vector2 velocity = characterScript.rigidbody.velocity;
            float x = Mathf.Clamp(velocity.x, -maxSpeed, maxSpeed);
            float y = Mathf.Clamp(velocity.y, -maxSpeed, maxSpeed);
            characterScript.rigidbody.velocity = new Vector2(x,y);
        }
        if (!Input.GetButton("Fire1"))
        {
            if (inInteraction)
            {
                childCollider.enabled = false;
                ReleaseObject(moveableObject);
                moveableObject = null;
                inInteraction = false;
            }
        }

    }

    void SetGrib(float amount)
    {

        if (GameHandler.godMode) return;
     //   float a =Mathf.Clamp(Input.GetAxis("Horizontal") + Input.GetAxis("Vertical"), -1,1);

     //   if (!characterScript.isGrounded) a = 1.2f;
     //   a = Mathf.Abs(a);

        SetStamina();
        if (isPulling && gribStrength >= 0)
        {
            gribStrength -= amount;
        }
        else if(gribStrength < 100)
        {
           if(characterScript.isGrounded) gribStrength += amount * 2;
        }

        if(gribStrength <= 0)
        {
           // inCooldown = true;
        }
        if(gribStrength > 20)
        {
            inCooldown = false;
        }


        if (isPulling) rigidbody.mass = 0.3f;
            else rigidbody.mass = 5;
    }

    IEnumerator Dash()
    {

        yield return new WaitForSeconds(1);
    }


    float shakeStrength;
    void Shake()
    {
        Vector2 shakeposition = new Vector2(spritePosition.x + (Random.insideUnitSphere.x * 0.02f * 2 * shakeStrength), spritePosition.y + Random.insideUnitSphere.x * 0.01f);
        sprite.transform.localPosition = Vector2.Lerp(sprite.transform.localPosition, shakeposition, 0.5f);
    }

    public void PlayAudio(AudioClip clip, bool restart)
    {
        if (!audioSource.isPlaying || restart)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
       
    }
}
