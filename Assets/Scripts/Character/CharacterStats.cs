﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStats : MonoBehaviour
{
    public float health;

    [HideInInspector]
    public float curHealth;

    [HideInInspector]
    public CharacterScript characterScript;
    public ParticleSystem poofParticles;
    Controls controls;
    public PauseHandler pauseHandler;

    [Tooltip("The color of the character when hit by an enemy")]
    public Color damageColor;
    public Color baseColor;

    public Image healthBar;

    public string walksprites = "Graphics/Sprites/Character/Knight Character/";
    public string idleSprites = "Graphics/Sprites/Character/Knight Character/";
    public string jumpingSprites = "Graphics/Sprites/Character/Knight Character/";
    public string fallingSprites = "Graphics/Sprites/Character/Knight Character/";
    public string dragSprites = "Graphics/Sprites/Character/Knight Character/";
    public string scaredSprites = "Graphics/Sprites/Character/Knight Character/";

    // Start is called before the first frame update
    void Start()
    {
        curHealth = health;
        SetHealthbar();

        controls = GetComponent<Controls>();
        characterScript = GetComponent<CharacterScript>();
        if (!pauseHandler) Debug.LogWarning("No Pause handler has been set! Don't forget to do this, Thomas!");
    }

    // Update is called once per frame
    void Update()
    {
        ChangeSprites();
        SetArmOffset();
    }

    void SetHealthbar()
    {
        healthBar.transform.localScale = new Vector2(Mathf.Clamp(1 / health * curHealth,0,1), 1f); 
    }

    public void ReceiveDamage(float damage)
    {
        if (GameHandler.godMode) return;

        curHealth -= damage;
        StartCoroutine(HitAnimation());
        SetHealthbar();

        characterScript.voiceSource.Stop();
        characterScript.PlayVoice(characterScript.hurt);

        if (curHealth <= 0)
        {
            StartCoroutine(DeathAnimation());
        }
    }

    float flashAmount;
    IEnumerator HitAnimation()
    {
        flashAmount = 0;
        while (flashAmount <= 1f)
        {
            yield return new WaitForSeconds(0.02f);
            flashAmount += 0.1f;
            characterScript.headSprite.material.SetFloat("_FlashAmount", flashAmount);
            characterScript.bodySprite.material.SetFloat("_FlashAmount", flashAmount);
        }
        while (flashAmount >= 0f)
        {
            yield return new WaitForSeconds(0.02f);
            flashAmount -= 0.1f;
            characterScript.headSprite.material.SetFloat("_FlashAmount", flashAmount);
            characterScript.bodySprite.material.SetFloat("_FlashAmount", flashAmount);
        }
        characterScript.headSprite.material.SetFloat("_FlashAmount", 0);
        characterScript.bodySprite.material.SetFloat("_FlashAmount", 0);
        flashAmount = 0;
    }

    IEnumerator DeathAnimation()
    {
        poofParticles.Play();
        pauseHandler.Death();
        yield return new WaitForSeconds(0.1f);
        poofParticles.gameObject.transform.SetParent(null);
        gameObject.SetActive(false);
    }

    void SetArmOffset()
    {
        if (characterScript.spriteAnimation.SpriteLocation == dragSprites)
        {
            characterScript.armOffset = new Vector2(-0.3902045f, 0.12f);
        }
        else
        {
            characterScript.armOffset = new Vector2(-0.2702045f, characterScript.armOffset.y);
        }

        characterScript.curArmOffset = (characterScript.spriteAnimation.spriteRenderer.flipX) 
                ? new Vector2(-characterScript.armOffset.x, characterScript.armOffset.y)
                : new Vector2(characterScript.armOffset.x, characterScript.armOffset.y);

    }
    void ChangeSprites()
    {
        if (controls.isScared && controls.grounded)
        {
            if (characterScript.spriteAnimation.SpriteLocation != scaredSprites)
            {
                characterScript.voiceSource.Stop();

                if (characterScript.spriteAnimation.SpriteLocation == fallingSprites)
                    characterScript.PlayVoice(characterScript.hurt);

          
                characterScript.spriteAnimation.ChangeSpritesLocation(scaredSprites, true);
                characterScript.headSprite.enabled = false;
                characterScript.armSprite.enabled = false;
            }
            return;
        }

        if (controls.swordScript.isPulling)
        {
            characterScript.armSprite.enabled = false;
            characterScript.armLine.enabled = true;
            Vector2 pos = (Vector2)transform.position + characterScript.distanceJoint.anchor;

            //if(Vector2.Distance(characterScript.sword.transform.position, pos) > 0.5f)
            //{

            //}

            float velocity = Mathf.Abs(characterScript.rigidbody.velocity.x) + Mathf.Abs(characterScript.rigidbody.velocity.y);
            if (Vector2.Distance(characterScript.sword.transform.position, pos) > 0.70f || !characterScript.isGrounded)
            {
                if (characterScript.spriteAnimation.SpriteLocation != dragSprites)
                {
                    characterScript.PlayVoice(characterScript.surprise);
                    characterScript.spriteAnimation.ChangeSpritesLocation(dragSprites, true);
                    characterScript.headSprite.enabled = false;
                }
            }else if (characterScript.spriteAnimation.SpriteLocation != idleSprites)
            {
                characterScript.spriteAnimation.ChangeSpritesLocation(idleSprites, true);
                characterScript.headSprite.enabled = true;
            }

            return;
        }

        if (!controls.grounded)
        {
            characterScript.armOffset = new Vector2(-0.2702045f, 0.17f);
            if (!controls.swordScript.isPulling)
            {
                characterScript.armSprite.enabled = true;
                characterScript.armLine.enabled = false;
                if (characterScript.rigidbody.velocity.y > .1f && characterScript.spriteAnimation.SpriteLocation != fallingSprites)
                {
                    characterScript.spriteAnimation.ChangeSpritesLocation(fallingSprites, true);
                    characterScript.headSprite.enabled = false;
                }
                else if(characterScript.spriteAnimation.SpriteLocation != fallingSprites)
                {
                    characterScript.PlayVoice(characterScript.fear);
                    characterScript.spriteAnimation.ChangeSpritesLocation(fallingSprites, true);
                    characterScript.headSprite.enabled = false;
                }
            }
        }
        else if (!characterScript.canMove && characterScript.isGrounded)
        {
            if (characterScript.spriteAnimation.SpriteLocation != idleSprites)
            {
                characterScript.voiceSource.Stop();

                //if (characterScript.spriteAnimation.SpriteLocation == fallingSprites)
                //    characterScript.PlayVoice(characterScript.hurt);

                characterScript.armSprite.enabled = true;
                characterScript.headSprite.enabled = true;
                characterScript.spriteAnimation.ChangeSpritesLocation(idleSprites, true);
        
            }
        }
        else if (Mathf.Abs(characterScript.directionValue) == 1)
        {
            if(characterScript.spriteAnimation.SpriteLocation != walksprites)
            {

                characterScript.armSprite.enabled = false;
                characterScript.headSprite.enabled = true;
                characterScript.spriteAnimation.ChangeSpritesLocation(walksprites, true);
                characterScript.voiceSource.Stop();

                if (characterScript.spriteAnimation.SpriteLocation == fallingSprites)
                    characterScript.PlayVoice(characterScript.hurt);
            }
        }
    }
}
