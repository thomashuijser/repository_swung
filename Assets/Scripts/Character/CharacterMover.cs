﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CharacterMover : MonoBehaviour
{
    public CharacterPathfinder cpf;
    CameraScript cs;

    public Sprite[] freezeSprites;
    public Sprite[] walkSprites;
    public Sprite[] idleSprites;
    public Sprite[] unconsciousSprites;
    public Sprite[] Jumping;

    public Directions direction;
    public float movementSpeed;
    public bool isGrounded;
    SpriteAnimation spriteAnimation;

    public SpriteRenderer characterSprite;
    Vector2 spritePos;
    Vector2 velocity;

    [HideInInspector]
    public Rigidbody2D rb;

    public int reactionDelay;
    float curDelay;

    public Transform armCenter;
    Material material;

    public float cooldown;

    bool unconscious;
    bool spottedEnemy;

    void Start()
    {
        cpf = GetComponent<CharacterPathfinder>();
        rb = GetComponent<Rigidbody2D>();
        cs = Camera.main.GetComponent<CameraScript>();

        spriteAnimation = characterSprite.GetComponent<SpriteAnimation>();

        Flip();
        curDelay = reactionDelay;
        material = characterSprite.material;
        spritePos = characterSprite.transform.localPosition;

    }

    void Update()
    {
        Debug.Log("test");
      //  CameraScript.ClampObject(transform);
        Reactions();
        CheckGrounded();
    }

    void LateUpdate()
    {
       // CameraScript.ClampObject(transform);
    }

    //checks if an enemy is in sight
    bool SpotEnemies()
    {
        bool enemy = false;
        foreach (string tag in cpf.directionHitTag)
        {
            if (tag == "Enemy")
            {
                enemy = true;
              if(!unconscious) StartCoroutine(FreezeCharacter(freezeSprites, 5));
            }
        }
        spottedEnemy = enemy;
        return enemy;
    }


    void Reactions()
    {
        if (unconscious)
        {
            cs.screenSize = 2f;
            Shake();
            return;
        }
        cs.screenSize = 2.5f;
        SpotEnemies();

        if(isGrounded)
        MovementReaction();
    }

    void MovementReaction()
    {
        //if an object on layer default is 1 unit away from the character
        if (cpf.directionsValue[CalculateRayDirection(2)] < 1)
        {
            // if the tile infront of the character is a tile
            if (cpf.directionHitTag[CalculateRayDirection(2)] == "Tile")
            {
                //if there is a tile above, the player can't jump and will flip
                if (cpf.directionHitTag[CalculateRayDirection(1)] == "Tile" && cpf.directionsValue[CalculateRayDirection(1)] < 1.5f)
                {
                    if (Delay()) Flip();
                }
                else if(cpf.directionsValue[CalculateRayDirection(2)] < 1)
                {
                    //the character jumps
                   if(Delay()) Jump(5);
                }
            }
        }
        else
        {
            Move();
        }
    }
    void Flip()
    {
        characterSprite.flipX = !characterSprite.flipX;

        if (characterSprite.flipX)
        {
            direction = Directions.Right;
            armCenter.localPosition = new Vector2(0.244f, armCenter.localPosition.y);
        }
        else
        {
            direction = Directions.Left;
            armCenter.localPosition = new Vector2(-0.244f, armCenter.localPosition.y);
        }
    }

    void Jump(float force)
    {
        if (!isGrounded) return;
        rb.velocity = new Vector2(force / 4, force * (int)direction);
        spriteAnimation.ChangeSprites(Jumping, true);
        isGrounded = false;
    }

    void Move()
    {
        Vector2 desiredPosition = new Vector2(transform.position.x + (int)direction * movementSpeed, transform.position.y);
        rb.position = Vector2.SmoothDamp(rb.position, desiredPosition, ref velocity, 0.2f);
    }

    bool Delay()
    {
        curDelay -= 0.1f;
        if (curDelay <= 0)
        {
            curDelay = reactionDelay;
            return true;
        }
        else
        {
            return false;
        }
    }

    int CalculateRayDirection(int rayDirection)
    {
        if (direction == Directions.Right) return rayDirection;
        else return 8 - rayDirection;
    }

    //shakes the character
    void Shake()
    {
        Vector2 shakeposition = new Vector2(spritePos.x + Random.insideUnitSphere.x * 0.01f * 2, spritePos.y + Random.insideUnitSphere.x * 0.01f);
        characterSprite.transform.localPosition = Vector2.Lerp(characterSprite.transform.localPosition, shakeposition, 0.5f);
    }

    //freezes the character in a position with a sprite animation
    IEnumerator FreezeCharacter(Sprite[] sprites, float time)
    {
        spriteAnimation.ChangeSprites(freezeSprites, true);

        unconscious = true;
        float curCooldown = time;
        while (curCooldown > 0)
        {
            yield return new WaitForSeconds(1);
            curCooldown--;
        }

        if (!spottedEnemy)
        {
            spriteAnimation.ChangeSprites(walkSprites, true);
            unconscious = false;
        }

        characterSprite.transform.localPosition = spritePos;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Tile")
        {
            if (cpf.directionsValue[4] < 0.7f && !isGrounded) isGrounded = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (unconscious) return;
        if (collision.gameObject.tag == "Tile")
        {
            //knocks the character out
            //Debug.Log(rb.velocity.x + rb.velocity.y);
            
            if (rb.velocity.x + rb.velocity.y > 4 || rb.velocity.x + rb.velocity.y < -4)
            {
                FreezeCharacter(unconsciousSprites, 8);
            }
            else if (cpf.directionsValue[4] < 0.7f && !isGrounded)
            {
                spriteAnimation.ChangeSprites(walkSprites, true);
            }
        }
    }

    void CheckGrounded()
    {
        if (cpf.directionsValue[4] < 0.7f && !isGrounded)
        {
            isGrounded = true;

            if(spriteAnimation.frames != walkSprites)
            {
                spriteAnimation.ChangeSprites(walkSprites, true);
            }
        }
        else
        {
            if(spriteAnimation.frames != Jumping && cpf.directionsValue[4] > 0.7f)
                spriteAnimation.ChangeSprites(Jumping, true);
            isGrounded = false;
        }
    }

    void sprites()
    {
        if (!isGrounded)
        {
            spriteAnimation.ChangeSprites(Jumping, true);
        }
        else
        {
            spriteAnimation.ChangeSprites(walkSprites, false);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
       
    }
}
