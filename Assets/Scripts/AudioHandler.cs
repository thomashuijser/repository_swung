﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
       if(!source) source = GetComponent<AudioSource>();
    }
    
    public void PlayAudio()
    {
        source.Play();
    }

    public void ChangeAudio(AudioClip clip)
    {
        source.clip = clip;
    }
}
