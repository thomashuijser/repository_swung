﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{

    public GameObject loadingPage;
    public Animator animator;

    public GameObject loadPage;

    public GameObject secretPage;

    public Animator[] chapterButtons;

    public static bool firstLoadUp = true;
    public static bool startInChapter;

    public AudioClip music;

    public Slider mouseSlider;

    public GameObject chapterPage;

    public Text score;

    public int unlockedLevels;

    public Resolution[] resolutions;

    private void Awake()
    {
        
      // Screen.SetResolution(1920, 1080, true);


        if (firstLoadUp) firstLoadUp = false;
        else
        {
            animator.SetBool("FirstStartUp", false);
            loadingPage.SetActive(true);
        }

        if (startInChapter)
        {
            animator.SetBool("StartInChapters", true);
            animator.SetBool("Open Book", true);
            animator.SetBool("Turn Page", true);
            loadPage.SetActive(true);
            chapterPage.SetActive(true);
        }
        else
        {
            animator.SetBool("StartInChapters", false);
        }

        score.text = PlayerPrefs.GetString("PlayTime");

        //debug
      

        unlockedLevels = PlayerPrefs.GetInt("Unlocked");
        

        UnlockChapters();
    }
    public void SetScreenResolution(int x, int y)
    {

        Screen.SetResolution(x, y, true);
    }

   

    void UnlockChapters()
    {
        for(int i =0; i<= unlockedLevels; i++)
        {
            if (i != unlockedLevels)
                chapterButtons[i].gameObject.SetActive(true);

            else
                StartCoroutine(unlockChapterButton(i));
            
        }
    }

    public void SwitchFullScreen(bool value = true)
    {
        int s;
        if (value) s = 1; else s = 0;

        PlayerPrefs.SetInt("Fullscreen", s);
        PlayerPrefs.Save();


        Screen.fullScreenMode = FullScreenMode.MaximizedWindow;
        Screen.fullScreen = value;
    }

    IEnumerator unlockChapterButton(int i)
    {
        yield return new WaitForSeconds(1);
        chapterButtons[i].gameObject.SetActive(true);
        chapterButtons[i].SetBool("Spawn", true);

    }
    public void DebugUnlockAll()
    {
        PlayerPrefs.SetInt("Unlocked", 4);
        unlockedLevels = PlayerPrefs.GetInt("Unlocked");
        PlayerPrefs.Save();

        UnlockChapters();
    }

    public void DebugResetAll()
    {
        foreach(Animator a in chapterButtons)
        {
            a.gameObject.SetActive(false);
        }

        PlayerPrefs.SetInt("Unlocked", 0);
        unlockedLevels = PlayerPrefs.GetInt("Unlocked");
        PlayerPrefs.Save();

        UnlockChapters();
    }

    void SetPreferences()
    {
        int i = PlayerPrefs.GetInt("FirstRun");
        if(i == 0)
        {
            PlayerPrefs.SetFloat("Sensitivity", 0.5f);
            i = 1;
            PlayerPrefs.SetInt("FirstRun", i);
        }
        mouseSlider.value = PlayerPrefs.GetFloat("Sensitivity", 0.5f);

        PlayerPrefs.Save();
    }

    public void SetSensitivity(float value)
    {
        PlayerPrefs.SetFloat("Sensitivity", value);
        PlayerPrefs.Save();
    }
    // Start is called before the first frame update
    void Start()
    {

        SetPreferences();

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;

      StartCoroutine(MusicHandler.SwitchMusic(music, true));

       // SwitchFullScreen(false);

        if(PlayerPrefs.GetInt("FirstPlay") == 0)
        {
            PlayerPrefs.SetInt("FirstPlay", 1);
            PlayerPrefs.Save();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TurnPage(GameObject page)
    {
        animator.SetBool("Turn Page", true);
        page.SetActive(true);
    }

    public void ReturnPage(GameObject page)
    {
        animator.SetBool("Turn Page", false);
        StartCoroutine(returnPage(page));
    }

    public void turnloadPage(string level)
    {
        StartCoroutine(LoadAsynchronously(level));
        animator.SetBool("Load", true);
        loadPage.SetActive(true);
    }

    IEnumerator returnPage(GameObject page)
    {
        yield return new WaitForSeconds(1);
        page.SetActive(false);
    }

    IEnumerator LoadAsynchronously(string level)
    {
        yield return new WaitForSeconds(1);
        AsyncOperation operation = SceneManager.LoadSceneAsync(level);

        while (!operation.isDone)
        {
            Debug.Log(operation.progress);

            yield return null;
        }
    }

    public void SecretButton()
    {
        secretPage.SetActive(true);
        animator.gameObject.SetActive(false);
        StartCoroutine(secret());
    }

    public IEnumerator secret()
    {
        yield return new WaitForSeconds(51);
        animator.gameObject.SetActive(true);

    }

    public void OpenBook()
    {
        animator.SetBool("Open Book", true);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
