﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform target;
    public Transform target2;
    public Vector2 targetPosition;
    public bool followTarget;

    [HideInInspector]
    public bool overrideScreenZoom;
    

    [HideInInspector]
    public bool inChokepoint;

    public Vector3 cameraOffset;

    public float screenSize = 2.8f;

    [HideInInspector]
    public float curScreensize;

    public float delay;

    public static Camera fieldCam;
    public static bool lockPlayer;

    public float y;

    public static float characterDistance;

    public float Xmin;
    public float xMax;

    public float Ymin;
    public float Ymax;

    public SwordScript swordScript;

    public Noticepoint[] noticePoints;


    public static bool shakeCamera;
    Vector2 shakePos;

    private void Awake()
    {
        curScreensize = screenSize;
        fieldCam = GetComponent<Camera>();
        noticePoints = FindObjectsOfType<Noticepoint>();

        swordScript = target2.GetComponent<SwordScript>();
        
        //Followtarget(true);
    }
    private void Start()
    {
        fieldCam = GetComponent<Camera>();
    }

    private void Update()
    {
        if (shakeCamera)
        {
            Shake();
        }
        else
        {
            shakePos = Vector2.zero;
        }
    }
    void Shake()
    {
        shakePos = new Vector2(Random.insideUnitSphere.x * 0.02f * 2, Random.insideUnitSphere.y * 0.015f * 2);
    }

    public static IEnumerator ShakeForTime(float time)
    {
        shakeCamera = true;
        yield return new WaitForSeconds(time);
        shakeCamera = false;
    }

    private void OnDrawGizmosSelected()
    {

        Vector2 linePos = new Vector2(Xmin, transform.position.y - 10);
        Vector2 lineposTo = new Vector2(Xmin, transform.position.y + 10);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(linePos, lineposTo);

        Vector2 mlinePos = new Vector2(xMax, transform.position.y - 10);
        Vector2 mlineposTo = new Vector2(xMax, transform.position.y + 10);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(mlinePos, mlineposTo);


        Vector2 ylinePos = new Vector2(transform.position.x - 10, Ymin);
        Vector2 ylineposTo = new Vector2(transform.position.x + 10, Ymin);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(ylinePos, ylineposTo);


        Vector2 ymlinePos = new Vector2(transform.position.x - 10, Ymax);
        Vector2 ymlineposTo = new Vector2(transform.position.x + 10, Ymax);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(ymlinePos, ymlineposTo);

    }

    public static Vector2 ClampObject(Vector2 pos)
    {
        pos = fieldCam.WorldToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        return (Vector2)fieldCam.ViewportToWorldPoint(pos);
    }

    void LateUpdate()
    {
        if(followTarget && !GameHandler.isPaused)
        Followtarget(false);
    }

    public static void ShakeCamera(float amount)
    {

    }

    Vector2 GetNoticepoints()
    {
        Vector2 position = Vector2.zero;
        float amount = 0;

        foreach(Noticepoint n in noticePoints)
        {
            if(Vector2.Distance(GameHandler.playerTransform.position, (Vector2)n.transform.position + n.offset) <= n.distance)
            {
                position += (Vector2)n.transform.position;
                amount++;
            }
        }

        amount++;
        position += (Vector2)GameHandler.playerTransform.position;
        position = position / amount;
        return position;
    }

    public void SetCameraZoom(bool value, float zoom = 0)
    {
        overrideScreenZoom = value;
        if(value)curScreensize = zoom;
    }

    void Followtarget(bool instant)
    {
        float moveSpeed = 5.125f;
        if (!inChokepoint && swordScript.isPulling)
        {
            moveSpeed = 5.125f;

        }

        if (inChokepoint)
        {
            targetPosition = target2.position;
        }
        else
        {
            Vector2 nearNotice = GetNoticepoints();

            targetPosition = nearNotice;
            
        }

        float size = fieldCam.orthographicSize;
        Vector3 desiredPosition = new Vector3(Mathf.Clamp(targetPosition.x, Xmin + size, xMax - size),Mathf.Clamp(targetPosition.y, Ymin, Ymax), transform.position.z);
        desiredPosition += cameraOffset;
       

        if (!instant) transform.position = Vector3.MoveTowards(transform.position, desiredPosition, moveSpeed * delay * Time.deltaTime);
        else transform.position = desiredPosition;

        transform.position += (Vector3)shakePos;

        characterDistance = Vector2.Distance(target.position, target2.position);

        //if (!overrideScreenZoom && !inChokepoint)
        //{
        //    curScreensize = Vector2.Distance(target.position, target2.position) + 1.5f;
        //    curScreensize = Mathf.Clamp(curScreensize, 2.6f, 3.5f);
        //}
      
       Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, curScreensize, 0.125f);
    }

    public void ChangeTarget(Transform t)
    {
        target2 = t;
    }
}
