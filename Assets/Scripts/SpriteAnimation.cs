﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteAnimation : MonoBehaviour
{
    [HideInInspector]
    public SpriteRenderer spriteRenderer;

    [Tooltip("delay between the sprites")]
    public float delay;
    public Object[] frames;
    public bool playOnStart = true;
    public bool loop = false;
    public bool folder = true;
    public bool finishedNoLoop;
    public bool deactivateOnFinish;

    public bool delayBasedOnSprite;
    public float[] delayPerSprite;

    public bool alterPosition;
    public GameObject posObject;
    public Vector3[] positions;

    public int curFrame = 0;

    bool pause;

    [Tooltip("The file location of the used sprites")]
    public string SpriteLocation;

    private void OnValidate()
    {
        if (Resources.LoadAll(SpriteLocation, typeof(Sprite)) != null && folder)
        {
            SetFrameSprites();
        }
    }
    
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if(folder)SetFrameSprites();

        if (playOnStart) StartCoroutine(PlayAnimation());
    }

    void SetFrameSprites()
    {
       frames = Resources.LoadAll(SpriteLocation, typeof(Sprite));
    }

    public void ChangeSpritesLocation(string location, bool resetFrame)
    {
        SpriteLocation = location;
        SetFrameSprites();
        spriteRenderer.sprite = (Sprite)frames[0];
        if (resetFrame) curFrame = 0;
        finishedNoLoop = false;
    }

    public void ChangeSprites(Sprite[] sprites, bool resetFrame)
    {
        frames = sprites;
        if (resetFrame)
        {
            curFrame = 0;
            spriteRenderer.sprite = (Sprite)frames[0];
        }

        else if (curFrame > sprites.Length - 1)
        {
            Debug.LogWarning("SpriteAnimation: the curframe is out of range for the new sprites array, resetting current frame to 0!");
            curFrame = 0;
        }
        finishedNoLoop = false;
    }

    public void PauseAnimation(bool value, bool reset)
    {
        pause = value;
        if (reset) curFrame = 0;
    }

    IEnumerator PlayAnimation()
    {
        while (true)
        {
            spriteRenderer.sprite = (Sprite)frames[curFrame];
            if (alterPosition) posObject.transform.localPosition = positions[curFrame];

            if (delayBasedOnSprite) yield return new WaitForSeconds(delayPerSprite[curFrame]);
            else yield return new WaitForSeconds(delay);

            if (!pause)
            {
                if (curFrame < frames.Length - 1) curFrame++;
                else if (loop)
                {
                    curFrame = 0;
                    finishedNoLoop = false;
                }
                else
                {
                    finishedNoLoop = true;
                    if (deactivateOnFinish) gameObject.SetActive(false);
                }

            }
        }
    }

}
