﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noticepoint : MonoBehaviour
{
    public float distance;
    public Vector2 offset;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawSphere((Vector2)transform.position + offset, distance);
    }
}
