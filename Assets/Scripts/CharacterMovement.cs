﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharacterMovement : MonoBehaviour
{
    public float[] directions;
    public float maxRaycastDistance;

    [Range(-1, 1)]
    public int startDirection;
    int movingDirection;
    bool isGrounded;

    public float movementSpeed;
    public float speedMultiplayer;
    [Tooltip("the delay when a reaction to an event is triggerd")]
    public float reactionDelay;
    float curReactionDelay;
    public float jumpStrength = 1;

    [Tooltip("if set to true, the character can jump when it is in collision with a wall")]
    public bool canWallJump;

    public bool isKnockedOut;

    public Rigidbody2D characterRigidbody;

    public LayerMask layermask;

    public bool lockCharacter;

    public SpriteRenderer characterSprite;
    public Sprite[] walkingSprites;
    public Sprite[] jumpingSprites;
    public Sprite[] idleSprites;

    public GameObject armCenter;
    public GameObject swordObject;
    public GameObject armObject;


    bool spottedEnemy;


    // Start is called before the first frame update
    void Start()
    {
        directions = new float[8];
        characterRigidbody = GetComponent<Rigidbody2D>();
        movingDirection = startDirection;
        curReactionDelay = reactionDelay;
        FlipCharacter();
    }

    // Update is called once per frame
    void Update()
    {
        CenterPosition(armObject.transform, armCenter.transform.position, swordObject.transform.position);
        if (lockCharacter) return;
        Charactermovement();
        AI();
        CheckForce();
    }

    //raycasts into multiple directions
    void Charactermovement()
    {
        int angle = 90;

        for (int i = 0; i < directions.Length; i++)
        {
            directions[i] = Raycasting(VectorFromAngle(angle), transform.position);
            angle -= 45;
        }

        //NEEDS OPTIMIZING FOR TESTING ONLY changes the sprite
    if(movementSpeed == 0)
        {
            characterSprite.GetComponent<SpriteAnimation>().ChangeSprites(idleSprites, true);
        }
        else if (movementSpeed == 1)
        {
            characterSprite.GetComponent<SpriteAnimation>().ChangeSprites(walkingSprites, false);
        }
    }

    void PathFinder()
    {
        if (directions[CalculateRayDirection(2)] < 1f * speedMultiplayer)
        {
            if(directions[CalculateRayDirection(1)] < 1f)
            {
                if (Delay())
                {
                    movingDirection = -movingDirection;
                    FlipCharacter();
                }
            }
            else 
            Jump(jumpStrength);
        }
    }

    void AI()
    {
        // set movingDirection to 0 to stop the character from moving
        Move(movingDirection);

        //checks if a tile is located infront of the character
        if (directions[CalculateRayDirection(2)] < 1f * speedMultiplayer)
        {

            //checks if the wall is to high, if so don't jump
            if (directions[CalculateRayDirection(1)] < 1.7f)
            {
                if (Delay())
                {
                    movementSpeed = 0;
                    movingDirection = -movingDirection;
                    FlipCharacter();
                }
            }
            else
            {
                Jump(jumpStrength);
            }
        }

        //stops the character from moving there isn't a ground close enough to the next tile
        else if (directions[CalculateRayDirection(3)] > 0.6f && !RaycastBetweenAngles(4, CalculateRayDirection(3), 10, new Vector3(transform.position.x + movingDirection, transform.position.y, transform.position.z))
            )
        {
            movementSpeed = 0;
            if (Delay())
            {
                movingDirection = -movingDirection;
                FlipCharacter();
            }
        }
        else
        {
            movementSpeed = 1;
        }
    }
    
    //flips the character
    void FlipCharacter()
    {
        if (movingDirection == 1)
        {
            characterSprite.flipX = true;
            armCenter.transform.localPosition = new Vector2(0.244f, armCenter.transform.localPosition.y);
        }
        else if (movingDirection == -1)
        {
            characterSprite.flipX = false;
            armCenter.transform.localPosition = new Vector2(-0.244f, armCenter.transform.localPosition.y);

        }
    }

    //reduces the delay until it hits zero
    bool Delay()
    {
        curReactionDelay -= 0.01f;
        if (curReactionDelay <= 0)
        {
            curReactionDelay = reactionDelay;
            return true;
        }
        else
        {
            return false;
        }
    }

    //moves the character
    void Move(int direction)
    {
        Vector2 desiredPosition = new Vector2(transform.position.x + direction * movementSpeed, transform.position.y);
        characterRigidbody.position = Vector2.MoveTowards(characterRigidbody.position, desiredPosition, (0.02f * speedMultiplayer));
    }

    int CalculateRayDirection(int rayDirection)
    {
        if (movingDirection == 1) return rayDirection;
        else return 8 - rayDirection;
    }

    void Jump(float force)
    {
        if (!isGrounded) return;
        characterRigidbody.velocity = new Vector2(force/4, force);
        isGrounded = false;
    }

    void MoveSpecifiedAmount(float amount)
    {
        Vector3 desiredPosition = new Vector2(transform.position.x + amount, transform.position.z);
        while (transform.position != desiredPosition)
        {
            characterRigidbody.position = Vector2.MoveTowards(characterRigidbody.position, desiredPosition, 0.05f);
        }
    }

    float Raycasting(Vector2 direction, Vector3 position)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, direction, maxRaycastDistance, layermask);

        if (hit.collider != null)
        {

            if (hit.collider.tag == "Tile")
            {
                if (hit.distance < 4f)
                    Debug.DrawRay(position, direction * hit.distance, Color.green);
                else Debug.DrawRay(position, direction * hit.distance, Color.yellow);
                return hit.distance;
            }
            else if (hit.collider.tag == "Enemy")
            {
                Debug.DrawRay(position, direction * hit.distance, Color.cyan);
            }
            else
            {
                //can add other collision here, like when an enemy is nearby
                Debug.DrawRay(position, direction * 10, Color.red);
            }
        }
        else
        {
            Debug.DrawRay(position, direction * 10, Color.red);
        }
        return maxRaycastDistance * 10;
    }

    Vector2 VectorFromAngle(float theta)
    {
        Vector2 dir = (Quaternion.Euler(0, 0, theta) * Vector2.right);
        return dir;
    }

    //checks if the character is in collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Tile")
        {
            if (directions[4] < 0.7f && !isGrounded || canWallJump) isGrounded = true;
        }
    }


    //sends a number of raycasts in between an angle
    bool RaycastBetweenAngles(int firstAngle, int secondAngle, int rayCount, Vector3 position)
    {
        int angle = 90 - (firstAngle * 45);
        int angle2 = -(secondAngle * 45) + 90;

        bool hashitTarget = false;
        for (int i = 0; i <= rayCount; i++)
        {
            angle += (angle2 - angle) / rayCount;
            if (Raycasting(VectorFromAngle(angle), position) < 10f) hashitTarget = true;
        }
        return hashitTarget;
    }

    //checks if the force on the character is higher then an amount. If so, the character changes it's direction path
    void CheckForce()
    {
       if(characterRigidbody.velocity.x > 2)
        {
            movingDirection = 1;
        }
        else if (characterRigidbody.velocity.x < -2)
        {
            movingDirection = -1;
        }
    }

    //centers the position inbetween two positions
    void CenterPosition(Transform t, Vector2 a, Vector2 b)
    {
        t.position = Vector2.Lerp(a,b, 0.5f);
    }

    public IEnumerator Stagger(int cooldown)
    {
        lockCharacter = true;
        yield return new WaitForSeconds(cooldown);
        lockCharacter = false;
    }


}
