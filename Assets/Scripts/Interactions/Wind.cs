﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Rigidbody2D rb = collision.transform.GetComponent<Rigidbody2D>();
            collision.transform.GetComponent<Controls>().gravityModifier = 0;
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y + Time.deltaTime, -1,4));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.transform.GetComponent<Controls>().gravityModifier = 1;
        }
        }
}
