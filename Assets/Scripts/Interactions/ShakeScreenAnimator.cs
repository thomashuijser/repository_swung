﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeScreenAnimator : MonoBehaviour
{
   public void shakeScreen(float shakeTime)
    {
        StartCoroutine(CameraScript.ShakeForTime(shakeTime));
    }

    IEnumerator Shaking(float shakeTime)
    {
        CameraScript.shakeCamera = true;
        yield return new WaitForSeconds(shakeTime);
        CameraScript.shakeCamera = false;
    }
}
