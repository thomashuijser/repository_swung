﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : MonoBehaviour
{
    public ParticleSystem[] fire;
    public Collider2D damageCollider;

    float duration;
    public float pauseLength;
    public bool playOnStart = true;

    [Range(0,1)]
    public float rotationSpeed;

    public BoxCollider2D boxCollider;
    

    // Start is called before the first frame update
    void Start()
    {
        duration = fire[0].main.duration;
       if(playOnStart) StartCoroutine(Loop());
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
        Damage();
    }

    void Rotate()
    {
        transform.Rotate(0, 0, 50 * Time.deltaTime * rotationSpeed);
    }

    void Damage()
    {
        RaycastHit2D hit;
        hit = Physics2D.Raycast(transform.position, transform.up);
        
      //  if (hit.transform == null) return;
    }

    IEnumerator Loop()
    {
        while (true)
        {
            
            foreach(ParticleSystem f in fire)
            {
                f.Play();
            }
            yield return new WaitForSeconds(duration + pauseLength);

        }
    }
}
