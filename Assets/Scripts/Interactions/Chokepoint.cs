﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chokepoint : MonoBehaviour
{
    public float screenSize;
    public bool lockCamera = true;
    public bool useFinishTrigger;
    public bool destroyOnFinish = true;
    public bool setCharacterScared;

    [Tooltip("sets the player still when chokepoint is started")]
    public bool reduceForce;
    Vector2 screenOffset;
    CameraScript cameraScript;
    bool started;
    

    public Transform player;
    public Transform sword;

    public bool xOnly;

    [Range(0, 10)]
    public float triggerDistance;
    public float finishTriggerDistance;

    public Vector2 triggerOffset;
    public Vector2 finishTriggerOffset;
    Vector2 triggerPoint;
    Vector2 finishPoint;

    [Tooltip("when these objects are disabled the chokepoint is finished")]
    public GameObject[] FinishObjects;

    public GameObject[] deactivateOnStart;
    public GameObject[] activateOnStart;
    public bool deactivateOnFinish  = true;
    public GameObject[] activateOnFinish;

    public float activateDelay;

    public bool isTutorial;
    public bool unlockAfterTutorial;
    public string tutorialText;

    public bool endGame;

    public PauseHandler pauseHandler;

    public bool activatePacked;
    public Packed packed;

    public float endTime;

    public AudioClip music;

    public Animator animator;

    public bool lockChokepoint = true;

    public bool stopMusic;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 0.5f);
        
        float height = screenSize * 2;
        float width = height * Screen.width / Screen.height;
        
       if(lockCamera) Gizmos.DrawCube((Vector2)transform.position, new Vector3(width, height, 1));

        Gizmos.color = new Color(1, 0, 0, 0.5f);

        Vector2 triggerpos = new Vector2(transform.position.x + triggerOffset.x, transform.position.y + triggerOffset.y);
        Gizmos.DrawSphere(triggerpos, triggerDistance);

        if (useFinishTrigger)
        {
            Gizmos.color = new Color(0, 0, 1, 0.5f);
            Vector2 finishTriggerpos = new Vector2(transform.position.x + finishTriggerOffset.x, transform.position.y + finishTriggerOffset.y);
            Gizmos.DrawSphere(finishTriggerpos, finishTriggerDistance);
        }
    }

    bool CheckFinishObjects()
    {

        foreach (GameObject g in FinishObjects)
        {
            if (g.activeSelf) return true;
        }

        return false;
    }

    IEnumerator ActivateObjects()
    {
        yield return new WaitForSeconds(activateDelay);

        foreach (GameObject g in activateOnStart)
        {
            g.SetActive(true);
        }

        started = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        triggerPoint = new Vector2(transform.position.x + triggerOffset.x, transform.position.y + triggerOffset.y);
        finishPoint = new Vector2(transform.position.x + finishTriggerOffset.x, transform.position.y + finishTriggerOffset.y);
        cameraScript = Camera.main.GetComponent<CameraScript>();

        pauseHandler = GameObject.Find("Canvas").GetComponent<PauseHandler>();

        characterControls = player.GetComponent<Controls>();
    }

    // Update is called once per frame

    Controls characterControls;
    void Update()
    {



        if (started)
        {
            if (setCharacterScared)
            {
                characterControls.addScaredTime();
            }
            
        if(FinishObjects.Length > 0)
        {
            if (!CheckFinishObjects())
            {
                EndChokepoint();
            }
        }else if (PlayerDistance(finishPoint) <= finishTriggerDistance && started && useFinishTrigger)
        {
            EndChokepoint();
        }
        }

        if (PlayerDistance(triggerPoint) <= triggerDistance && !started)
        {
            StartChokepoint();
            return;
        }
    }

    float PlayerDistance(Vector2 point)
    {
        if (xOnly)
        {
            point = new Vector2(point.x, player.position.y);
        }
        return Vector2.Distance(point, player.position);
    }
    
    void StartChokepoint()
    {
        GameHandler.inChokepoint = true;

        foreach(GameObject g in deactivateOnStart)
        {
            g.SetActive(false);
        }

        if (animator)
        {
            animator.SetBool("Activate", true);
        }

        if (lockChokepoint)
        {
            GameHandler.lockChokepoint = true;
        }
        else
        {
            GameHandler.lockChokepoint = false;
        }

        if (music)
        {
          StartCoroutine(MusicHandler.SwitchMusic(music, true));
        }
        if (activatePacked)
        {
            packed.SetTarget(player);
        }

        if(endTime > 0)
        {
            StartCoroutine(EndAfterTime());
        }

        if (lockCamera)
        {
            cameraScript.ChangeTarget(transform);
            cameraScript.curScreensize = screenSize;
            cameraScript.inChokepoint = true;
        }

        if(reduceForce) player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

      StartCoroutine(ActivateObjects());

        if (isTutorial)StartCoroutine(Tutorial(0.5f));
        
    }

    IEnumerator Tutorial(float time)
    {
        yield return new WaitUntil(() => PauseMenu.paused == false);
        yield return new WaitForSeconds(time);
        yield return new WaitForSeconds(1f);
        if(unlockAfterTutorial)EndChokepoint();
    }

    void EndChokepoint()
    {
        if (stopMusic)
        {
            MusicHandler.audioSource.Stop();
        }

        if (animator)
        {
            animator.SetBool("Deactivate", true);
        }

        GameHandler.inChokepoint = false;
        if (!endGame)
        {
            cameraScript.ChangeTarget(sword);
        cameraScript.curScreensize = cameraScript.screenSize;
        cameraScript.inChokepoint = false;
        //started = false;
       
        }

        if (endGame)
        {
            CharacterScript cs = player.GetComponent<CharacterScript>();
            cs.voiceSource.Stop();
            
            cs.PlayVoice(cs.pride);
            pauseHandler.WinGame();
        }

        foreach (GameObject g in activateOnFinish)
        {
            g.SetActive(true);
        }

        //temp. needs to be finish, but can be same menu for debug demo!
        if (destroyOnFinish) gameObject.SetActive(false);
    }

    IEnumerator EndAfterTime()
    {
        yield return new WaitForSeconds(endTime);
        EndChokepoint();
    }
}
