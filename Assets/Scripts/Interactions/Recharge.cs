﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recharge : MonoBehaviour
{
    public bool rechargeStamina;
    public bool rechargeHealth;
    public bool oneUseOnly;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            if (rechargeHealth)
            {
                CharacterStats c = collision.GetComponent<CharacterStats>();
                c.curHealth = c.health;
            }
            if (rechargeStamina)
            {
                CharacterScript s = collision.GetComponent<CharacterScript>();
                if (s.swordScript.isPulling)
                {
                    s.swordScript.gribStrength = 100;
                }
            }
            if (oneUseOnly) gameObject.SetActive(false);

        }
    }
}
