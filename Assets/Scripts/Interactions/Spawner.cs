﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public bool BeginOnActivate = true;
    public GameObject enemy;
    [Tooltip("sets the target automatically on spawn, the enemy will not have to spot the character first")]
    public bool autoSetTarget;
    public int maxEnemies;
    public float spawnDelay;

    public float velocity;

    public bool randomizeX;
    public float randomAmount;

    public bool restart;

    public bool playOnStart = true;

    int activeEnemies;

    public List<GameObject> activatedEnemies;
    // Start is called before the first frame update
    void Start()
    {
        // activatedEnemies.Clear();
        Debug.Log("Starting!");
     if(playOnStart)StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        if (restart)
        {
            StartCoroutine(Spawn());
            restart = false;
        }

        foreach(GameObject g in activatedEnemies)
        {
            if (!g.activeSelf)
            {
                activatedEnemies.Remove(g);
                return;
            }
        }
    }

    IEnumerator Spawn()
    {

        while (gameObject.activeSelf)
        {
            Vector2 spawnPos = transform.position;
            if (randomizeX)
            {
                spawnPos = new Vector2(transform.position.x + Random.Range(-randomAmount, randomAmount), transform.position.y);
            }
            GameObject e = Instantiate(enemy, spawnPos, Quaternion.identity);
            activatedEnemies.Add(e);

            if (velocity != 0) e.GetComponent<Rigidbody2D>().velocity = (transform.up * velocity);

            if (autoSetTarget) e.GetComponent<EnemyPathfinder>().target = GameHandler.playerTransform;
            if (maxEnemies != 0) yield return new WaitUntil(() => activatedEnemies.Count < maxEnemies);
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}
