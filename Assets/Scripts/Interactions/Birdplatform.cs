﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Birdplatform : MonoBehaviour
{
    [Range(0,1)]
    public float amplitude;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = transform.parent.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Idle();
        Debug.Log("test");
    }

    void Idle()
    {
        Vector2 pos = new Vector2(0, 0 + Mathf.Sin(Time.time) * amplitude);
        transform.localPosition = pos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            animator.SetBool("Start", true);
        }
    }
}
