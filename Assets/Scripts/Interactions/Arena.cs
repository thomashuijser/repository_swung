﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{

    public Spawner[] part1;
    public Spawner[] part2;
    public Spawner[] part3;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayArena());
    }

    IEnumerator PlayArena()
    {
        yield return new WaitForSeconds(4);
        part1[0].enabled = true;
        part1[1].enabled = true;
        yield return new WaitForSeconds(10);
        part1[2].enabled = true;
        yield return new WaitForSeconds(120);
        part1[0].spawnDelay = 5;
        part1[1].spawnDelay = 5;
        yield return new WaitForSeconds(120);
        part2[0].enabled = true;
        part2[1].enabled = true;
        yield return new WaitForSeconds(120);
        part2[0].spawnDelay = 5;
        yield return new WaitForSeconds(120);
        part3[0].enabled = true;
        part3[1].enabled = true;
        part1[0].spawnDelay = 2.5f;
        part1[1].spawnDelay = 2.5f;
        yield return new WaitForSeconds(120);
        part3[0].spawnDelay = 5;
        part3[1].spawnDelay = 5;
    }
}
