﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObject : MonoBehaviour
{

    public float health;
    public float shakeTime;

    [Tooltip("deactivates the collider on destroy")]
    public bool deactivateCollider;

    public GameObject[] activateOnDestroy;
    public GameObject[] deactivateOnDestroy;

    public SpriteRenderer[] sprites;

    public bool ignoreSword;
    Vector2 startPos;

    bool isShaking;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;

        sprites = GetComponentsInChildren<SpriteRenderer>();
    }


    private void Update()
    {
        if (isShaking) Shake();
    }
    public void ReceiveDamage(float damage)
    {
        health -= damage;

        if(health <= 0)
        {
            StartCoroutine(DestroyObject());
        }
        else
        {
            StartCoroutine(HitAnimation());
            StartCoroutine(StartShake());
        }
    }


    IEnumerator StartShake()
    {
        isShaking = true;
        yield return new WaitForSeconds(0.5f);
        isShaking = false;
    }
    void ShakeForTime(int time)
    {
        float curTime = time;

        while (curTime >= 0)
        {
            Debug.Log("SHAKE!");
            curTime -= Time.deltaTime;
            Shake();
        }
    }
    

    IEnumerator DestroyObject()
    {
        ShakeForTime(2);
        yield return new WaitForSeconds(0.1f);

        foreach (GameObject g in activateOnDestroy) g.SetActive(true);
        foreach (GameObject g in deactivateOnDestroy) g.SetActive(false);

        if (deactivateCollider) GetComponent<BoxCollider2D>().enabled = false;
    }


    float flashAmount;
    IEnumerator HitAnimation()
    {
        flashAmount = 0;
        while (flashAmount <= 1f)
        {
            yield return new WaitForSeconds(0.01f);
            flashAmount += 0.1f;

            foreach(SpriteRenderer s in sprites)
            {
                s.material.SetFloat("_FlashAmount", flashAmount);
            }
        }
        while (flashAmount >= 0f)
        {
            yield return new WaitForSeconds(0.01f);
            flashAmount -= 0.1f;

            foreach (SpriteRenderer s in sprites)
            {
                s.material.SetFloat("_FlashAmount", flashAmount);
            }
        }
        foreach (SpriteRenderer s in sprites)
        {
            s.material.SetFloat("_FlashAmount", 0);
        }
        flashAmount = 0;
    }


    void Shake()
    {
        Vector2 shakeposition = new Vector2(startPos.x + Random.insideUnitSphere.x * 0.02f * 2, startPos.y + Random.insideUnitSphere.x * 0.01f);
        transform.position = Vector2.Lerp(transform.position, shakeposition, 0.5f);
    }
}
