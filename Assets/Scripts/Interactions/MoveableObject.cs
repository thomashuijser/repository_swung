﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableObject : MonoBehaviour
{

    public SpriteRenderer sprite;
    Rigidbody2D rigidbody;
 
    public ParticleSystem particles;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public void PickUp(Transform parent)
    {
       if(particles) particles.Play();
        transform.SetParent(parent);
        rigidbody.isKinematic = true;
        transform.localPosition = new Vector2(0, 0.4f);
    }
    
    public void Release()
    {
        if (particles) particles.Play();
        transform.SetParent(null);
        rigidbody.isKinematic = false;
    }
}
