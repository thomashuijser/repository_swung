﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageRay : MonoBehaviour
{
    float damage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up);
        Debug.Log(hit.transform);
        Debug.DrawLine(hit.transform.position, transform.position, Color.red);
    }
}
