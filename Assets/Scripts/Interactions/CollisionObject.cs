﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionObject : MonoBehaviour
{
    Animator objectAnimation;
    
    public GameObject[] activateOnCollision;
    public GameObject[] deactivateOnCollision;

    [Tooltip("sets the animators (activate) & (deactivate)")]
    public Animator[] animators;

    public bool characterTriggerOnly;

    float collisionObjects;

    public bool deactivate = true;

    public bool lockPlayer;

    // Start is called before the first frame update
    void Start()
    {
      if(GetComponent<Animator>() != null) objectAnimation = GetComponent<Animator>();
    }

    void ActivateObject()
    {
        foreach (GameObject g in activateOnCollision)
        {
            g.SetActive(true);
        }

        foreach (GameObject g in deactivateOnCollision)
        {
            g.SetActive(false);
        }

        foreach (Animator a in animators)
        {
            a.SetBool("Activate", true);
            a.SetBool("Deactivate", false);
        }

        if(objectAnimation != null)
        {
            objectAnimation.SetBool("Activate", true);
            objectAnimation.SetBool("Deactivate", false);
        }
    }

    void DectivateObject()
    {
        foreach (GameObject g in activateOnCollision)
        {
            g.SetActive(false);
        }

        foreach (GameObject g in deactivateOnCollision)
        {
            g.SetActive(true);
        }

        foreach (Animator a in animators)
        {
            a.SetBool("Activate", false);
            a.SetBool("Deactivate", true);
        }

        if (objectAnimation != null)
        {
            objectAnimation.SetBool("Activate", false);
            objectAnimation.SetBool("Deactivate", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player"  || collision.tag == "MoveableObject" && !characterTriggerOnly)
        {
            if (lockPlayer)
            {
           //     collision.transform.parent = transform;
            }
            
            if(collisionObjects <= 0)
            {
                ActivateObject();
            }
            collisionObjects++;

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "MoveableObject" && !characterTriggerOnly && deactivate)
        {
            if (lockPlayer)
            {
               // collision.transform.parent = null;
            }

            collisionObjects--;

            if(collisionObjects <= 0)
            DectivateObject();
        }
    }
}
