﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageObject : MonoBehaviour
{
    public float minVelocity;
    public int damage;
    public bool damageEnemy;
    public bool damagePlayer;
    public bool requireVelocity;
    public bool isSword;
    [Tooltip("if true, the object gets knockback if it its a player")]
    public bool returnKnockback;

    public bool destroyChildOnly;
    public bool destroyOnImpact;
    public ParticleSystem blood;
    Rigidbody2D rb;

    [HideInInspector]
    public string hitTag;

    [Tooltip("the velocity added to the character that is hit")]
    public Vector2 velocity;

    [HideInInspector]
    public Transform origin;

    AudioSource audioSource;

    public bool ignoreFirstHit;
    bool firstHit = true;
    public bool hasAudio;

    public AudioClip projectileHit;
    public AudioClip CharacterHit;
    public AudioClip GroundHit;

    public GameObject activateOnDestroy;


    public float DamageMulti = 1;

    public float destroyTime;

    // Start is called before the first frame update
    void Start()
    {
        if (hasAudio) audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();

        if (destroyTime > 0) StartCoroutine(destroyEnd());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator destroyEnd()
    {
        yield return new WaitForSeconds(destroyTime);
        gameObject.SetActive(false);
    }

    void receiveKnockback(Vector2 knockback)
    {
        if (!GetComponent<Rigidbody2D>())
        {
            Debug.LogWarning("object needs rigidbody if it has to receive knockback!");
            return;
        }
        Rigidbody2D r = GetComponent<Rigidbody2D>();
        r.velocity = knockback * 5;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger) return;
        
        //if(collision.transform == origin && ignoreFirstHit || collision.transform.parent == origin && ignoreFirstHit)
        //{
        //    Debug.Log("hit origin");
        //    ignoreFirstHit = false;
        //    return;
        //}

        hitTag = collision.tag;
        float value = velocity.x + velocity.y;

        if (velocity.x + velocity.y >= minVelocity || velocity.x + velocity.y <= -minVelocity || !requireVelocity)
        {
            if (collision.tag == "Enemy" && damageEnemy)
            {
                if (damage == 0) return;
                if (hasAudio)
                {
                    PlayAudio(CharacterHit);
                }

                EnemyPathfinder em = collision.GetComponent<EnemyPathfinder>();
                EnemyStats es = collision.GetComponent<EnemyStats>();

                if (es.ignoreSword && isSword) return;

                es.ReceiveDamage(damage);// currently simply using damage because it's broken
                Vector2 direction = transform.position - em.transform.position;
                Vector2 svelocity = ((direction * 300)) * Mathf.Clamp(value, -0.8f, 0.8f);

                float maxDir = 2;
                direction = new Vector2(Mathf.Clamp(direction.x, -maxDir, maxDir), Mathf.Clamp(direction.y, -maxDir, maxDir));

                //add force
                if (em.receiveKnockback && velocity != Vector2.zero) em.rb.velocity = -(direction * 10);
                if (blood && !es.protect)
                {
                    blood.startColor = es.bloodColor;
                    blood.transform.position = Vector2.Lerp(transform.position, collision.transform.position, 0.5f);
                    blood.Play();
                }
            }
            else if (collision.tag == "Destructable Object")
            {
               
                DestructableObject dao = collision.GetComponent<DestructableObject>();

                if (dao.ignoreSword && isSword) return;

                dao.ReceiveDamage(damage);
            }
            else if (collision.tag == "Player" && damagePlayer)
            {
                if (damage == 0) return;

                if (hasAudio)
                {
                    PlayAudio(CharacterHit);
                }

                CharacterStats cs = collision.GetComponent<CharacterStats>();
                Vector2 direction = transform.position - cs.transform.position;
                cs.ReceiveDamage(damage * DamageMulti);
                cs.characterScript.rigidbody.velocity = -(direction * 15);

            }
            else if (collision.tag == "Projectile")
            {
                if (hasAudio)
                {
                    PlayAudio(projectileHit);
                }
                Vector2 direction = transform.position - collision.transform.position;
                collision.GetComponent<Rigidbody2D>().velocity = (-collision.GetComponent<Rigidbody2D>().velocity + direction);
                collision.GetComponent<Rigidbody2D>().gravityScale = (1 - DamageMulti) + 0.1f;
            }
            else if (collision.tag == "Tile")
            {
               if(GroundHit) PlayAudio(GroundHit);
            }
            }
        GameObject g = transform.parent.gameObject;

        if (destroyOnImpact)
        {
            if (activateOnDestroy)
            {
                activateOnDestroy.transform.parent = null;
                activateOnDestroy.SetActive(true);
            }

            if (destroyChildOnly)
            {
                gameObject.SetActive(false);
            }
            else{
              if(transform.parent.GetComponentInChildren<SpriteRenderer>())  transform.parent.GetComponentInChildren<SpriteRenderer>().transform.SetParent(collision.transform);
                transform.parent.gameObject.SetActive(false);
            }
            
            //Rigidbody2D rig = transform.parent.GetComponent<Rigidbody2D>();
            //rig.velocity = new Vector2(Random.Range(-5,5), Random.Range(-5,5));
            //transform.parent.GetComponent<Projectile>().enabled = false;
            //rig.angularVelocity = Random.Range(-5, 5);
            
            //rig.gravityScale = 1;
            //Debug.Log(collision.transform);

            enabled = false;
        }
    }

    void PlayAudio(AudioClip clip)
    {
      
            audioSource.clip = clip;
            audioSource.Play();
                
    }
}
