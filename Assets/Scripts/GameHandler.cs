﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{

    public static bool levelEnded;
    public static bool isPaused;
    public static bool levelWon;
    public static bool inTutorial;
    public static bool usingController;
    public static bool inLoading;
    public static bool inChokepoint;
    public static bool lockChokepoint = true;

    public static float playTime;
    public bool isFinalLevel;
    public Camera WorldView;

    public static Camera Cam;

    public static Transform swordTransform;
    public static Transform playerTransform;

    static GameObject key;

    public Level currentLevel;

    public bool isGod;
    public static bool godMode;

    // Start is called before the first frame update
    void Start()
    {
        key = GameObject.Find("Key");

        godMode = isGod;

        if (!inTutorial)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        Cam = WorldView;

        if(currentLevel.music && MusicHandler.audioSource.clip != currentLevel.music)
        {
            StartCoroutine(MusicHandler.SwitchMusic(currentLevel.music));
        }

        if (currentLevel.stopMusic)
            MusicHandler.audioSource.Stop();
    }


    public static IEnumerator EndFinalLevel()
    {
        yield return new WaitForSeconds(2);

    }


    // Update is called once per frame
    void Update()
    {
        playTime += Time.deltaTime;
    }

    public static void ActivateKey(bool value)
    {
        key.SetActive(value);
    }

    public static void SetKeyPosition(Vector2 position)
    {
        key.transform.position = position;
    }

    public static void FinishLevel(string nextLevel)
    {
        SceneManager.LoadScene(nextLevel);
    }

    private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
    private const string TWEET_LANGUAGE = "en";
    public static string descriptionParam;
    private string appStoreLink = "https://swung.itch.io/swung";

    public void ShareToTW(string linkParameter)
    {
        float t = GameHandler.playTime;
        int sec = (int)(t % 60);
        t /= 60;
        int minutes = (int)(t % 60);
        t /= 60;
        int hours = (int)(t % 24);

        string nameParameter = "I survived #swung arena-mode for " + hours + ":" + minutes + ":" + sec;
        Application.OpenURL(TWITTER_ADDRESS +
           "?text=" + WWW.EscapeURL(nameParameter + "\n" + descriptionParam + "\n" + "Get the Game:\n" + appStoreLink));
    }
}
