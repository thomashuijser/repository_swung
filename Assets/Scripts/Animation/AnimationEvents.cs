﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationEvents : MonoBehaviour
{
    public void LoadLevel(string level)
    {

        MenuScript.firstLoadUp = true;
        SceneManager.LoadScene(level);
    }

    public void DestroyGameObject(Transform gameobject)
    {
        Destroy(gameObject);
    }
}
