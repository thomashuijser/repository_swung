﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFilterHandler : MonoBehaviour
{

    public MeshRenderer scene;

    [Range(0,1)]
    public float greyScale;
    [Range(0,1)]
    public float Colorscale;
    [Range(0, 1)]
    public float tintScale;

    public Color tint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scene.material.SetFloat("_Greyscale", greyScale);
        scene.material.SetFloat("_Colorscale", Colorscale);


        Color color = Color.Lerp(Color.white, tint, tintScale);
        scene.material.SetColor("_ColorTint", color);
    }
}
