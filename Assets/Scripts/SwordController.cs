﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{
    public GameObject swordObject;
    SpriteRenderer swordSprite;
    ParticleSystem swordParticles;
    public Transform armTransform;
    CharacterMovement characterMovement;
    public float maxSwordDistance;
    DamageObject swordDamageObject;

    public Transform[] armdragSprites;
    public Transform[] swordDragSprites;

    [Range(0, 1)]
    public float mouseSensitivity;
    float swordVelocity;

    Vector2 moveToPosition;
    Vector2 swordPosition;

    int launchCount;
    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterMovement>();
        swordSprite = swordObject.GetComponentInChildren<SpriteRenderer>();
        swordPosition = swordSprite.transform.localPosition;
        swordDamageObject = GetComponentInChildren<DamageObject>();

        
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;
        Move();
        GetSwordVelocity();
        SetDragSprites(swordDragSprites);
    }


    void SetDragSprites(Transform[] transforms)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            swordDragSprites[i].position = Vector2.Lerp(swordDragSprites[i].position, swordSprite.transform.position, 0.3f / (i + 1));
            swordDragSprites[i].rotation = Quaternion.RotateTowards(swordDragSprites[i].rotation, swordDamageObject.transform.rotation, 10);
        }
    }
    //gets the current sword velocity
    void GetSwordVelocity()
    {
        swordVelocity = Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y");
    }

    //moves the sword towards the desired positions
    void Move()
    {
        float mouseX = swordObject.transform.localPosition.x + Input.GetAxis("Mouse X") * mouseSensitivity;

        float mouseY = swordObject.transform.localPosition.y + Input.GetAxis("Mouse Y") * mouseSensitivity;

        SetSwordVelocity();

        float outsideBounds = 0.2f;
        if (mouseX > (maxSwordDistance + outsideBounds) || mouseX < -(maxSwordDistance + outsideBounds) || mouseY > maxSwordDistance + outsideBounds || mouseY < -(maxSwordDistance + outsideBounds))
        {
            Shake();
        }
        else swordSprite.transform.localPosition = swordPosition;


        mouseX = Mathf.Clamp(mouseX, -maxSwordDistance, maxSwordDistance);
        mouseY = Mathf.Clamp(mouseY, -maxSwordDistance, maxSwordDistance);

        Vector2 desiredPosition = new Vector2(mouseX, mouseY);
        moveToPosition = desiredPosition;
        //moves sword towards desired position
        swordObject.transform.localPosition = Vector2.Lerp(swordObject.transform.localPosition, desiredPosition, 0.4f);
        Vector2 direction = desiredPosition;

        // rotate sword towards desired position unless right mousebutton is down
        if (!Input.GetMouseButton(1)) swordObject.transform.up = Vector2.Lerp(swordObject.transform.up, direction, 0.8f);
        armTransform.up = Vector2.Lerp(armTransform.transform.up, direction, 0.8f);

        if (Input.GetMouseButton(0))
        {
            Shake();
        }
        if (Input.GetMouseButtonUp(0))
        {
            LaunchCharacter(direction);
        }
    }


    //shakes the sword sprite
    void Shake()
    {
        Vector2 shakeposition = new Vector2(swordPosition.x + Random.insideUnitSphere.x * 0.04f * (1 * 2), swordPosition.y + Random.insideUnitSphere.x * 0.01f);
        swordSprite.transform.localPosition = Vector2.Lerp(swordSprite.transform.localPosition, shakeposition, 0.5f);
    }

    void LaunchCharacter(Vector2 direction)
    {
        launchCount++;
        float chargeCount = 10;

        float y;
        float x = direction.x;

        if (direction.y > 0) y = Mathf.Clamp(direction.y, 0.5f, 10);
        else y = direction.y;

        if (launchCount >= 3)
        {
            //release sword
        }

        characterMovement.characterRigidbody.velocity = new Vector2(x * chargeCount, y * chargeCount);
        StartCoroutine(characterMovement.Stagger(20));
    }

    void ReleaseSword()
    {
       
    }

    void SetSwordVelocity()
    {
        swordDamageObject.velocity = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
    }

    Vector2 VectorFromAngle(float theta)
    {
        Vector2 dir = (Quaternion.Euler(0, 0, theta) * Vector2.right);
        return dir;
    }
}
