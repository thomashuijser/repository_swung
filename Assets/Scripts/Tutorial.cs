﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    Animator animator;
    int current;

    public MeshRenderer scene;
    public Color color;

    public bool setTimeScale = true;
    public bool changeColor = true;

    public float maxCurrent = 5;

    AudioSource audioSource;
    public AudioClip[] audioClips;

    private void Awake()
    {
        if (setTimeScale)
        {
            GameHandler.inTutorial = true;
        }
    }

    private void Start()
    {
        animator = GetComponent<Animator>();

        if (!changeColor) return;
        scene.material.SetColor("_ColorTint", color);
        scene.material.SetFloat("_Greyscale", 0);
        scene.material.SetFloat("_Colorscale", 1);

        if (setTimeScale)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }

        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(current < maxCurrent && setTimeScale)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        if (current == maxCurrent)
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            current = 6;
        }
    }
    public void Next()
    {
        current++;
        if (audioSource)
        {
            audioSource.clip = audioClips[current];
            audioSource.Play();
        }

        animator.SetInteger("Page",current);
    }

    public void Return()
    {
        current--;
        animator.SetInteger("Page", current);
    }
}
