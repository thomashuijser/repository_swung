﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteAnimation))]
[ExecuteInEditMode]
public class SpriteAnimationEditor : Editor
{
    SerializedProperty 
        SpriteLocation,
        frames,
        curFrame,
        playOnStart,
        loop,
        delay,
        folder,
        alterPosition,
        posObject,
        positions,
        delayBasedOnSprite,
        delayPerSprite,
        deactivateOnFinish;


    // Start is called before the first frame update
    void OnEnable()
    {
        SpriteLocation = serializedObject.FindProperty("SpriteLocation");
        frames = serializedObject.FindProperty("frames");
        curFrame = serializedObject.FindProperty("curFrame");
        playOnStart = serializedObject.FindProperty("playOnStart");
        loop = serializedObject.FindProperty("loop");
        delay = serializedObject.FindProperty("delay");
        folder = serializedObject.FindProperty("folder");
        alterPosition = serializedObject.FindProperty("alterPosition");
        posObject = serializedObject.FindProperty("posObject");
        positions = serializedObject.FindProperty("positions");
        delayBasedOnSprite = serializedObject.FindProperty("delayBasedOnSprite");
        delayPerSprite = serializedObject.FindProperty("delayPerSprite");
        deactivateOnFinish = serializedObject.FindProperty("deactivateOnFinish");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(folder);
        EditorGUILayout.PropertyField(alterPosition);
        if (alterPosition.boolValue) {
            EditorGUILayout.PropertyField(posObject);
            EditorGUILayout.PropertyField(positions, true);
        }
        if (folder.boolValue) EditorGUILayout.PropertyField(SpriteLocation);
        else EditorGUILayout.PropertyField(frames, true);
        EditorGUILayout.PropertyField(playOnStart);
        EditorGUILayout.PropertyField(loop);

        EditorGUILayout.PropertyField(delayBasedOnSprite);
        if (delayBasedOnSprite.boolValue)
        {
           EditorGUILayout.PropertyField(delayPerSprite, true);
        }
        else EditorGUILayout.PropertyField(delay);

        EditorGUILayout.PropertyField(deactivateOnFinish);
        curFrame.intValue = EditorGUILayout.IntSlider(curFrame.intValue, 0, frames.arraySize - 1);
        if (!SpriteLocation.stringValue.Contains("Graphics"))
        {
            SpriteLocation.stringValue = "Graphics/";
        }
        serializedObject.ApplyModifiedProperties();
        if (frames.arraySize <= 0) {
            GUILayout.Label("No sprites found in selected file location!");
            return;
        }
        frames = serializedObject.FindProperty("frames");
        Sprite sprite = (Sprite)frames.GetArrayElementAtIndex(curFrame.intValue).objectReferenceValue;
        Texture2D texture = AssetPreview.GetAssetPreview(sprite);
        EditorGUILayout.LabelField(frames.GetArrayElementAtIndex(curFrame.intValue).objectReferenceValue.name);
        GUILayout.Label(texture);
    }
}
