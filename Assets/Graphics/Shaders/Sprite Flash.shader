﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/DefaultColorFlash" {
	Properties{
	  _MainTex("Texture", 2D) = "white" {}
	  _FogColor("Fog Color", Color) = (0.3, 0.4, 0.7, 1.0)
	  _ColorTint("Tint", Color) = (1.0, 0.6, 0.6, 1.0)
	}
		SubShader{
		  Tags { "RenderType" = "Opaque" }
		  CGPROGRAM
		  #pragma surface surf Lambert finalcolor:mycolor vertex:myvert
		  struct Input {
			  float2 uv_MainTex;
			  half fog;
		  };

	  fixed4 _ColorTint;

		  void myvert(inout appdata_full v, out Input data)
		  {
			  UNITY_INITIALIZE_OUTPUT(Input,data);
			  float4 hpos = UnityObjectToClipPos(v.vertex);
			  hpos.xy /= hpos.w;
			  data.fog = min(1, dot(hpos.xy, hpos.xy)*0.5);
		  }

		  fixed4 _FogColor;
		  void mycolor(Input IN, SurfaceOutput o, inout fixed4 color)
		  {
			  fixed3 fogColor = _FogColor.rgb;
			  #ifdef UNITY_PASS_FORWARDADD
			  fogColor = 0;
			  #endif
			  color.rgb = lerp(color.rgb, fogColor, IN.fog);
			  color *= _ColorTint;
		  }
		  sampler2D _MainTex;
		  void surf(Input IN, inout SurfaceOutput o) {
			  half4 c = tex2D(_MainTex, IN.uv_MainTex);
			  o.Albedo = dot(c.rgb, float3(0.3, 0.5, 1.11));
			  o.Alpha = c.a;
		  }
		  ENDCG
	  }
		  Fallback "Diffuse"
}
